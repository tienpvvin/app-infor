/**
 * @format
 */

import { AppRegistry } from 'react-native';
import codePush from 'react-native-code-push';
import 'react-native-gesture-handler';
import App from './App';
import { name as appName } from './app.json';

const codePushOptions = {
    updateDialog: false,
    checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
    installMode: codePush.InstallMode.IMMEDIATE
};

let RootComponent = codePush(codePushOptions)(App);

if (__DEV__) {
    RootComponent = App;
}

AppRegistry.registerComponent(appName, () => RootComponent);
