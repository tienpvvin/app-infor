## Utils

-   ### AsyncStorageUtils -> [AsyncStorageUtils.ts][src/utils/AsyncStorageUtils.ts] -> [Demo][]

```
save
get
remove
getObject
saveObject
clear
```

-   ### DateTimeUtils -> [DateTimeUtils.ts][src/utils/DateTimeUtils.ts] -> [Demo][]

```
millis2String
string2Millis
string2Date
date2String
date2Millis
getNowMilis
getNowString
getNowDate
getWeeOfToday
isToday
getTimeSpanByNow
```

-   ### RegexUtils -> [RegexUtils.ts][src/utils/RegexUtils.ts] -> [Demo][]

```
isEmail
isURL
```
