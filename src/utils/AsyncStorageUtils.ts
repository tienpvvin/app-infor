import AsyncStorage from '@react-native-community/async-storage';

/**
 * Key of AsyncStorage
 */
export enum StorageKey {
    SAMPLE_KEY = 'SAMPLE_KEY',
    PROJECT = 'PROJECT',
    TOKEN = 'TOKEN'
}

export class AsyncStorageUtils {
    /**
     * Lưu lại giá trị vào AsyncStorage
     * @param key
     * @param value
     */
    static save(key: StorageKey, value: string): void {
        AsyncStorage.setItem(key, value);
    }

    /**
     * Lấy giá trị từ trong AsyncStorage
     * @param key
     */
    static async get(key: StorageKey): Promise<string | null> {
        return AsyncStorage.getItem(key);
    }

    /**
     * Xóa giá trị đã lưu trong AsyncStorage
     * @param key
     */
    static async remove(key: StorageKey): Promise<void> {
        return AsyncStorage.removeItem(key);
    }

    /**
     * Get đối tượng/mảng đã lưu từ AsyncStorage
     * @param key
     */
    static async getObject<T>(key: StorageKey): Promise<T | null> {
        const value = await AsyncStorage.getItem(key);
        if (!value) return null;

        return JSON.parse(value);
    }

    /**
     * Lưu lại đối tượng hoặc 1 mảng vào AsyncStorage
     * @param key
     * @param value
     */
    static saveObject<T>(key: StorageKey, value: T): void {
        AsyncStorage.setItem(key, JSON.stringify(value));
    }

    static async clear(callback?: (error?: Error) => void): Promise<void> {
        await AsyncStorage.clear(callback);
    }
}
