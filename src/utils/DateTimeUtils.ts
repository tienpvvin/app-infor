/* eslint-disable @typescript-eslint/no-dupe-class-members */
/* eslint-disable max-classes-per-file */
import moment from 'moment';

export class DateTimeUtils {
    static DEFAULT_DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss';

    static TimeConstants = class {
        static MSEC = 1;

        static SEC = 1000;

        static MIN = 60000;

        static HOUR = 3600000;

        static DAY = 86400000;
    };

    /**
     * Milliseconds to the formatted time string.
     * @param millis The milliseconds.
     * @param pattern The pattern of date format. Default DEFAULT_DATE_FORMAT
     */
    static millis2String(
        millis: number,
        pattern = DateTimeUtils.DEFAULT_DATE_FORMAT
    ): string {
        return moment.unix(millis).format(pattern);
    }

    /**
     * Formatted time string to the milliseconds.
     * @param time The formatted time string.
     * @param pattern The pattern of date format. Default DEFAULT_DATE_FORMAT
     * @return the milliseconds
     */
    static string2Millis(
        time: string,
        pattern = DateTimeUtils.DEFAULT_DATE_FORMAT
    ): number {
        return moment(time, pattern).unix() * 1000;
    }

    /**
     * Formatted time string to the date.
     * @param time The formatted time string.
     * @param pattern The pattern of date format. Default DEFAULT_DATE_FORMAT
     */
    static string2Date(
        time: string,
        pattern = DateTimeUtils.DEFAULT_DATE_FORMAT
    ): Date {
        return moment(time, pattern).toDate();
    }

    /**
     * Date to the formatted time string.
     * @param date The date.
     * @param pattern The pattern of date format. Default DEFAULT_DATE_FORMAT
     */
    static date2String(
        date: Date,
        pattern = DateTimeUtils.DEFAULT_DATE_FORMAT
    ): string {
        return moment(date).format(pattern);
    }

    /**
     * Date to the milliseconds.
     * @param date The date.
     * @return the milliseconds
     */
    static date2Millis(date: Date): number {
        return moment(date).unix() * 1000;
    }

    /**
     * Milliseconds to the date.
     * @param millis The milliseconds.
     */
    static millis2Date(millis: number): Date {
        return moment.unix(millis).toDate();
    }

    /**
     * Return the current time in milliseconds.
     */
    static getNowMilis(): number {
        return Date.now();
    }

    /**
     * Return the current formatted time string.
     */
    static getNowString(pattern?: string): string {
        return this.millis2String(Date.now(), pattern);
    }

    /**
     * Return the current date.
     */
    static getNowDate(): Date {
        return new Date();
    }

    /**
     * Return wee of today in millis
     */
    static getWeeOfToday(): number {
        return moment(new Date().setHours(0, 0, 0, 0)).unix() * 1000;
    }

    /**
     * Return whether it is today.
     * @param time The time in millis/Date/formatted date
     * @param format The pattern of input time format if there is formatted date. Default DEFAULT_DATE_FORMAT
     */
    static isToday(time: number | Date | string, format?: string): boolean {
        let millis = 0;
        if (time instanceof Date) {
            millis = time.getTime();
        } else if (typeof time === 'number') {
            millis = time;
        } else if (typeof time === 'string') {
            millis = this.string2Millis(time, format);
        }
        const wee = this.getWeeOfToday();
        return millis >= wee && millis < wee + DateTimeUtils.TimeConstants.DAY;
    }

    /**
     * Return whether it is today.
     * @param time The time in millis/Date/formatted date
     * @param pattern The pattern of input time format if there is formatted date. Default DEFAULT_DATE_FORMAT
     */

    static getTimeSpanByNow(
        time: number | Date | string,
        pattern?: string
    ): string {
        let millis = 0;
        if (time instanceof Date) {
            millis = time.getTime();
        } else if (typeof time === 'number') {
            millis = time;
        } else if (typeof time === 'string') {
            millis = this.string2Millis(time, pattern);
        }

        const now = Date.now();
        const span = now - millis;
        if (span < 0) {
            throw Error('Thời gian lớn hơn thời gian hiện tại');
        }

        if (span < DateTimeUtils.TimeConstants.SEC) {
            return 'Just now';
        }

        if (span < DateTimeUtils.TimeConstants.MIN) {
            return `${Math.floor(
                span / DateTimeUtils.TimeConstants.SEC
            )} seconds ago`;
        }

        if (span < DateTimeUtils.TimeConstants.HOUR) {
            return `${Math.floor(
                span / DateTimeUtils.TimeConstants.MIN
            )} minutes ago`;
        }

        if (span < DateTimeUtils.TimeConstants.DAY) {
            return `${Math.floor(
                span / DateTimeUtils.TimeConstants.HOUR
            )} hours ago`;
        }
        return this.millis2String(millis, pattern);
    }
}
