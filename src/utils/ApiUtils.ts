/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable prefer-promise-reject-errors */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable no-undef */
// eslint-disable-next-line no-unused-vars
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import { GlobalVariable } from 'constans/global-variable';

const REQ_TIMEOUT = 15 * 1000;

const instance = axios.create({
    baseURL: 'http://crm-shop.yez.vn/api/',
    timeout: REQ_TIMEOUT
});

instance.interceptors.request.use((_config) => requestHandler(_config));

const requestHandler = (request: AxiosRequestConfig) => {
    if (__DEV__) {
        // console.log(`Request API: ${request.url}`, request.params, request.data);
        console.log(`Request API: ${request.url}`, request);
    }
    return request;
};

instance.interceptors.response.use(
    (response) => successHandler(response),
    (error) => errorHandler(error)
);

const errorHandler = (error: any) => {
    if (__DEV__) {
        console.log(error);
    }
    return Promise.reject({ ...error });
};

const successHandler = (response: AxiosResponse) => {
    if (__DEV__) {
        console.log(`Response API: ${response.config.url}`, response);
    }
    return response.data;
};

async function fetch<ReqType, ResType>(
    url: string,
    params?: ReqType,
    customHeaders: any = {}
): Promise<ResType> {
    const headers = getHeader(customHeaders);
    return instance.get(url, { params, headers });
}

async function post<ReqType, ResType>(
    url: string,
    data?: ReqType,
    customHeaders: any = {}
): Promise<ResType> {
    const headers = getHeader(customHeaders);
    return instance.post(url, { ...data }, { headers });
}

async function postForm<ReqType, ResType>(
    url: string,
    data?: ReqType,
    customHeaders: any = {}
): Promise<ResType> {
    const headers = getHeader(customHeaders);
    return instance.post(url, data, { headers });
}

async function put<ReqType, ResType>(
    url: string,
    data?: ReqType,
    customHeaders: any = {}
): Promise<ResType> {
    const headers = getHeader(customHeaders);
    return instance.put(url, { ...data }, { headers });
}

function getHeader(customHeaders: any = {}): any {
    return {
        Cookie:
            'sessionid=5272491734%3AjyfHQ2NP5wXqIR%3A6; csrftoken=bun5Cgm2RSCtBuuxGFS3FPGEgqqX8fD7; shbid=9356; shbts=1617790590.5627553; mid=YG2GfgAEAAHDxA1xWtHAYyXpW6J6; ds_user_id=5272491734; ig_did=77AAAD27-70AD-40DB-9D87-15B5E3C9F001; rur=RVA; fbsr_124024574287414=EEbI23gYCaukKOrWpJGBlIMr3yci0xrYF0OUDGAjyMI.eyJ1c2VyX2lkIjoiMTAwMDEwNjkxNTAxMjMxIiwiY29kZSI6IkFRQ2RqaUZRQWU4YlVlSmxzT2x1MjB1SjF2VVktRXJVckZSOVE3QklxSUZUM0RVMkdpcW1pVTB4bld4Z3VrUGtJRTNreHQtNWxCZWxxdnhfYldHSTJlQzgyT2lCV1dadE9JVG9UNTlaMFV5NFJPMUFsVDdJT1dSYzlfYU93Y1VUTV9ySnFqR3dwM1hBTE1wZVFXaGhrQWhuSW1Wcks2NTkxRVplQ2QycFphZURmQWkxYmk2Z2lRcnFOVm1WS0Q4THBLaGtSdGZoLUZpZTZkOHFUZmVGaEdNeFZLYUp4dlRzWFk0eWZqSlJNWU5xSGF0UXlpdUhiNGUzMFRuRmNwb0tIZTlqNkFfb2lUa21KWUk0c0hOYW5VclA4MHRQRXZVNWhOY21NRmI2STZrcTN1NUZsN1k1SnNsdUcxRW1acms2dmZGcHFmNzFiaFJVTURMb1lNaEhGN21jIiwib2F1dGhfdG9rZW4iOiJFQUFCd3pMaXhuallCQURIN0d4dXZxNHEydEQ3WkJnc2UzM1dHQkViUENKYnludlRhMGFMQmJ5dE1KUXF5T3Brb2paQUVBZlpCWkFrSzBXT011b0NrTTVKRDZJbnRaQ1RLUDRPTWZsc3lhZ2pwcHVUZ2NicFZjREdRbWF3c204emwzYnlROFVaQXl3elRaQmhoc2pURDFHYVFzWkNiWkNrd0NrT0pqN2k1UzB0NWNWdXk0VmxaQlhwZEViM05aQVhnRVc4NHFVWkQiLCJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImlzc3VlZF9hdCI6MTYxNzc5MDU2NH0',
        'User-Agent':
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4',
        'x-csrftoken': 'bun5Cgm2RSCtBuuxGFS3FPGEgqqX8fD7',
        'x-ig-app-id': 936619743392459,
        'sec-ch-ua': `Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99`,
        referer: 'https://www.instagram.com/',
        authorization:`Bearer ${GlobalVariable.token}`
        // ...customHeaders
    };
}

const ApiUtils = { fetch, post, put, postForm };
export default ApiUtils;
