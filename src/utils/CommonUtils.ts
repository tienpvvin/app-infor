import ToastComponent, { ToastType } from 'libraries/toast/toast.component';
import { Keyboard, Platform } from 'react-native';
import { translate } from 'res/languages';

export class Utils {}

export const hideKeyboard = (): void => {
    Keyboard.dismiss();
};

export const Limit = {
    PRODUCT: 20
};

export const isIOS = Platform.OS === 'ios';

export const isAndroid = Platform.OS === 'android';

export const isEmpty = (str?: string): boolean => {
    return !str || str.trim().length === 0;
};

export const moveItem = (arr: any[], from: number, to: number): any[] => {
    return arr.splice(to, 0, arr.splice(from, 1)[0]);
};

export const showAlertWarning = (key: string): void => {
    ToastComponent.show({
        type: ToastType.WARNING,
        message: translate(key)
    });
};

export const showAlertSuccess = (key: string): void => {
    ToastComponent.show({
        type: ToastType.SUCCESS,
        message: translate(key)
    });
};

export const showAlertError = (key: string): void => {
    ToastComponent.show({
        type: ToastType.ERROR,
        message: translate(key)
    });
};
