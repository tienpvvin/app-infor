/* eslint-disable max-classes-per-file */
export class RegexUtils {
    static RegexConstants = class {
        static REGEX_EMAIL =
            '^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$';

        static REGEX_URL = '[a-zA-z]+://[^\\s]*';
    };

    static isEmail(input: string): boolean {
        const re = new RegExp(RegexUtils.RegexConstants.REGEX_EMAIL);
        return re.test(input);
    }

    static isURL(input: string): boolean {
        const re = new RegExp(RegexUtils.RegexConstants.REGEX_URL);
        return re.test(input);
    }
}
