import { IProject } from 'types/project.type';

export const DataSound = [
    {
        name: 'None'
    },
    {
        name: 'Home Clock',
        audio: 'home_clock.mp3'
    },
    {
        name: 'Rain distant thunder',
        audio: 'rain_distant_thunder.mp3'
    },
    {
        name: 'Rain Window',
        audio: 'rain_window.mp3'
    }
];

export const DATA_COLOR_PROJECT = [
    '#5EAAA8',
    '#78290F',
    '#90BE6D',
    '#8FCEB5',
    '#BB6BD9',
    '#E8C371',
    '#2F80ED'
];

export const DATA_PROJECT_DEFAULT: IProject[] = [
    {
        _id: '1',
        name: 'Work',
        color: '#5EAAA8',
        totalTask: 3
    },
    {
        _id: '2',
        name: 'Meet',
        color: '#78290F',
        totalTask: 5
    },
    {
        _id: '3',
        name: 'Home',
        color: '#E8C371',
        totalTask: 10
    },
    {
        _id: '4',
        name: 'Personal',
        color: '#BB6BD9',
        totalTask: 8
    }
];
