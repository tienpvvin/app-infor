import { IProject } from "types/project.type";
import { DateTimeUtils } from "utils/DateTimeUtils";

interface GlobalVariableParams {
  pomodoroLength: number;
  projects: IProject[];
  token: string | null;
}

export const GlobalVariable: GlobalVariableParams = {
  pomodoroLength: DateTimeUtils.TimeConstants.MIN * 25,
  projects: [],
  token: "",
};
