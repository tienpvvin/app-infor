/* eslint-disable react/no-access-state-in-setstate */
import { GlobalVariable } from 'constans/global-variable';
import BaseText from 'libraries/container/base-text';
import BaseIcon from 'libraries/icon/base-icon';
import ModalYesNoComponent from 'libraries/modal/modal-yes-no';
import CountDownTime from 'libraries/time/count-down-time';
import * as React from 'react';
import { LayoutAnimation, StyleSheet, View } from 'react-native';
import * as Progress from 'react-native-progress';
import { translate } from 'res/languages';
import PopupSelectSound from './popup-select-sound';

interface Props {}

interface State {
    timerDefault: number;
    timer: number;
    isPlay: boolean;
}

const WIDTH_ROUND = 256;

export default class HomeTimerProgress extends React.Component<Props, State> {
    private timer: any;

    private refModalNo = React.createRef<ModalYesNoComponent>();

    private refSound = React.createRef<PopupSelectSound>();

    constructor(props: Props) {
        super(props);
        this.state = {
            timerDefault: GlobalVariable.pomodoroLength / 1000,
            timer: GlobalVariable.pomodoroLength / 1000,
            isPlay: false
        };
    }

    public componentWillUnmount(): void {
        this.onClearInterval();
    }

    private onClearInterval = (): void => {
        if (this.timer) clearInterval(this.timer);
    };

    private onPlay = (): void => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);
        this.setState({ isPlay: !this.state.isPlay }, this.onPlayTimer);
    };

    private onPlayTimer = () => {
        if (!this.state.isPlay) {
            this.onClearInterval();
            return;
        }

        this.timer = setInterval(() => {
            this.setState({
                timer: Math.max(0, this.state.timer - 1)
            });
        }, 1000);
    };

    private onClose = (): void => {
        this.refModalNo?.current?.open({ content: 'home.contentModal' });
    };

    private onYes = (): void => {
        this.onClearInterval();
        this.setState({ timer: this.state.timerDefault, isPlay: false });
    };

    private onSelectSound = (): void => {
        this.refSound?.current?.open?.();
    };

    private getProgress = (): number => {
        const { timer, timerDefault } = this.state;
        return (timerDefault - timer) / timerDefault;
    };

    private renderBottom = () => {
        const { isPlay } = this.state;
        return (
            <View style={styles.viewBottom}>
                <BaseIcon
                    name="ic_music"
                    onPress={this.onSelectSound}
                    style={styles.bkaBtn}
                    width={25}
                />
                <BaseIcon
                    name={isPlay ? 'ic_pause' : 'ic_play'}
                    onPress={this.onPlay}
                    style={[styles.bkaBtn, { width: 60, height: 60 }]}
                    width={38}
                />
                {isPlay ? (
                    <BaseIcon
                        name="ic_close"
                        onPress={this.onClose}
                        style={styles.bkaBtn}
                        width={25}
                    />
                ) : (
                    <View style={{ width: 44 }} />
                )}
            </View>
        );
    };

    public render(): React.ReactNode {
        const { timer } = this.state;
        return (
            <>
                <View style={styles.container}>
                    <Progress.Circle
                        size={WIDTH_ROUND}
                        progress={this.getProgress()}
                        unfilledColor="#DFEEEE"
                        borderWidth={0}
                        color="#5EAAA8"
                        thickness={6}
                        strokeCap="round"
                    />

                    <View style={styles.content}>
                        <CountDownTime
                            txtTime={{ marginTop: 20 }}
                            until={timer}
                        />
                        <BaseText style={styles.session}>
                            {`5/10 ${translate('home.sessions')}`}
                        </BaseText>
                    </View>

                    {this.renderBottom()}
                </View>
                <ModalYesNoComponent
                    ref={this.refModalNo}
                    onPressYes={this.onYes}
                />

                <PopupSelectSound ref={this.refSound} />
            </>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 30
    },
    content: {
        position: 'absolute',
        top: 0,
        width: WIDTH_ROUND,
        height: WIDTH_ROUND,
        borderRadius: WIDTH_ROUND,
        alignItems: 'center',
        justifyContent: 'center'
    },
    session: {
        marginTop: 20
    },
    viewBottom: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 24,
        width: WIDTH_ROUND - 38,
        justifyContent: 'space-between'
    },
    bkaBtn: {
        width: 44,
        height: 44,
        borderRadius: 44,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: 'rgba(0, 0, 0, 0.12)',
        shadowOffset: { width: 0.1, height: 2 },
        shadowOpacity: 3,
        shadowRadius: 2,
        elevation: 2
    }
});
