/* eslint-disable react/sort-comp */
import { DataSound } from 'constans/data';
import ButtonBase from 'libraries/button/button-base';
import TouchableOpacity from 'libraries/button/touchable-opacity';
import BaseText from 'libraries/container/base-text';
import BaseIcon from 'libraries/icon/base-icon';
import * as React from 'react';
import {
    FlatList,
    ListRenderItem,
    Modal,
    StyleSheet,
    View
} from 'react-native';
import Sound from 'react-native-sound';
import { translate } from 'res/languages';
import R from 'res/R';
import { DimensionUtils } from 'utils/DimensionUtils';

interface Props {}

interface State {
    visible: boolean;
    audioName: string;
}

export default class PopupSelectSound extends React.Component<Props, State> {
    private whoosh?: Sound = undefined;

    constructor(props: Props) {
        super(props);
        this.state = {
            visible: false,
            audioName: DataSound[0].name || ''
        };
    }

    public open(): void {
        this.setState({ visible: true });
    }

    private onClose = (): void => {
        this.setState({ visible: false });
    };

    private onPlaySound = (item: any) => () => {
        if (this.state.audioName === item.name) return;
        this.setState({ audioName: item.name });

        this.whoosh?.stop();
        this.whoosh = undefined;

        if (item.name === DataSound[0].name) {
            return;
        }

        this.whoosh = new Sound(item.audio, Sound.MAIN_BUNDLE, (error) => {
            if (error) {
                console.log('Sound Play - Error: ', error);
                return;
            }
            this.whoosh?.play();
            this.whoosh?.setNumberOfLoops(-1);
        });
    };

    private keyExtractor = (item: any): string => item.name;

    private renderItem: ListRenderItem<any> = ({ item }) => {
        const isCheck = this.state.audioName === item.name;

        return (
            <TouchableOpacity
                style={styles.btn}
                onPress={this.onPlaySound(item)}
            >
                <BaseText style={styles.name}>{item.name}</BaseText>
                {isCheck ? (
                    <BaseIcon name="ic_check" />
                ) : (
                    <View style={{ height: 22 }} />
                )}
            </TouchableOpacity>
        );
    };

    public render(): React.ReactNode {
        return (
            <Modal
                animationType="fade"
                transparent
                visible={this.state.visible}
            >
                <View style={[styles.modalWrap]}>
                    <TouchableOpacity
                        onPress={this.onClose}
                        style={styles.modalOverlay}
                        activeOpacity={1}
                    />

                    <View style={[styles.modalView]}>
                        <BaseText style={styles.title} fontStyle="bold">
                            {translate('home.whiteNoise')}
                        </BaseText>

                        <FlatList
                            data={DataSound}
                            keyExtractor={this.keyExtractor}
                            renderItem={this.renderItem}
                            style={styles.fsStyle}
                            extraData={this.state.audioName}
                        />

                        <ButtonBase
                            style={styles.btnStyle}
                            text="save"
                            onPress={this.onClose}
                        />
                    </View>
                </View>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    modalWrap: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: R.colors.black60p
    },
    modalOverlay: {
        flex: 1,
        position: 'absolute',
        width: '100%',
        height: '100%'
    },
    modalView: {
        backgroundColor: R.colors.white,
        zIndex: 10,
        marginHorizontal: 12,
        borderRadius: 14,
        alignItems: 'center'
    },
    title: {
        color: '#001C31',
        fontSize: 20,
        marginTop: 16,
        marginBottom: 10
    },
    fsStyle: {
        width: '100%',
        maxHeight: 300
    },
    btn: {
        paddingVertical: 15,
        marginHorizontal: 30,
        borderBottomWidth: 1,
        borderBottomColor: '#F2F2F2',
        flexDirection: 'row',
        alignItems: 'center'
    },
    name: {
        fontSize: 15,
        flex: 1
    },
    btnStyle: {
        borderRadius: 8,
        marginTop: 20,
        marginBottom: 20,
        width: DimensionUtils.getScreenWidth() - 80,
        height: 44
    }
});
