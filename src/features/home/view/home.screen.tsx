/* 
  Created by dungnt at 03-04-2021 14:39:01
  Screen Màn hình trang chủ
*/

import BaseText from 'libraries/container/base-text';
import ContainerHeader from 'libraries/container/container-header';
import * as React from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { translate } from 'res/languages';
import { HomeScreen } from 'routing/screen-name';
import { pushToPage } from 'routing/service-navigation';
import { HomeAdapter, HomeParamsRoute, HomeProps, HomeState } from '../model';
import HomeTimerProgress from './components/home-timer-progress';

export default class HomeContainer extends React.PureComponent<
    HomeProps,
    HomeState
> {
    static start(params?: HomeParamsRoute) {
        pushToPage(HomeScreen, params);
    }

    private adapter: HomeAdapter;

    constructor(props: HomeProps) {
        super(props);
        this.adapter = new HomeAdapter(this);
    }

    public componentDidMount(): void {}

    public render(): React.ReactNode {
        return <ContainerHeader title="Danh sách cửa hàng" />;
    }
}

// styles
const styles = StyleSheet.create({
    btnHeader: {
        position: 'absolute',
        right: 8,
        top: 5,
        zIndex: 1
    },
    icHeader: {
        width: 30,
        height: 30
    },
    current: {
        color: '#828282',
        fontSize: 16,
        textAlign: 'center',
        marginTop: 25
    },
    stayFocus: {
        color: '#000000',
        fontSize: 14,
        textAlign: 'center',
        marginTop: 90
    }
});
