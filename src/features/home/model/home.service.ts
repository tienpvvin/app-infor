/* 
  Created by dungnt at 03-04-2021 14:39:01
  Screen Màn hình trang chủ
*/
import ApiUtils from 'utils/ApiUtils';

const apiName = {};

const ID_DEFAULT = '5272491734%3AjyfHQ2NP5wXqIR%3A6';

export const apiGetFollowing = () => {
    return ApiUtils.fetch(
        `https://www.instagram.com/graphql/query/?query_hash=3dec7e2c57367ef3da3d987d89f9dbc8&variables=%7B%22id%22%3A%225272491734%22%2C%22include_reel%22%3Atrue%2C%22fetch_mutual%22%3Afalse%2C%22first%22%3A24%7D`
    );
};

// 'https://www.instagram.com/graphql/query/?query_hash=3dec7e2c57367ef3da3d987d89f9dbc8&variables=%7B%22id%22%3A%2247348210102%22%2C%22include_reel%22%3Atrue%2C%22fetch_mutual%22%3Afalse%2C%22first%22%3A24%7D'
