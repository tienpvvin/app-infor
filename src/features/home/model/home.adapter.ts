/* 
  Created by dungnt at 03-04-2021 14:39:01
  Screen Màn hình trang chủ
*/
import HomeContainer from '../view/home.screen';

export class HomeAdapter {
    private view: HomeContainer;

    constructor(view: HomeContainer) {
        this.view = view;
    }
}
