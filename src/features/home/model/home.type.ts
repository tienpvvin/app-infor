
/* 
  Created by dungnt at 03-04-2021 14:39:01
  Screen Màn hình trang chủ
*/

import { RouterType } from "types/routes.type";

export interface HomeParamsRoute {}

export interface HomeProps {
  route: RouterType<HomeParamsRoute>
}

export interface HomeState {}

