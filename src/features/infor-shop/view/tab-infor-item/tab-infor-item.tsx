import CreateItemContainer from "features/create-item/view/create-item.screen";
import * as React from "react";
import { View, StyleSheet, Text, TouchableOpacity } from "react-native";
import { DimensionUtils } from "utils/DimensionUtils";

export interface Props {}

export default class TabInforItem extends React.PureComponent<Props, any> {
  constructor(props: Props) {
    super(props);
  }

  onAddItem = () => {
    CreateItemContainer.start();
  };
  public render() {
    return (
      <View style={{ flex: 1, alignItems: "center" }}>
        <TouchableOpacity
          style={styles.btnAdd}
          activeOpacity={1}
          onPress={this.onAddItem}
        >
          <View style={styles.view1}>
            <View
              style={[styles.view1, { transform: [{ rotate: "90deg" }] }]}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  btnAdd: {
    backgroundColor: "#FFF",
    width: 64,
    height: 64,
    borderRadius: 32,
    shadowColor: "rgba(0, 0, 0, 0.16)",
    shadowOffset: { width: 0.1, height: 2 },
    shadowOpacity: 6,
    shadowRadius: 2,
    elevation: 2,
    position: "absolute",
    bottom: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  view1: {
    width: 5,
    height: 37,
    backgroundColor: "#FF7D00",
    borderRadius: 7,
  },
});
