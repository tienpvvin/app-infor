/* 
  Created by TienPv at 08-05-2021 10:49:27
  Screen Thong tin ve shop
*/

import BaseText from "libraries/container/base-text";
import ContainerHeader from "libraries/container/container-header";
import * as React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { TabView } from "react-native-tab-view";
import R from "res/R";
import { InforShopScreen } from "routing/screen-name";
import { pushToPage } from "routing/service-navigation";
import { DimensionUtils } from "utils/DimensionUtils";
import {
  EInforShop,
  InforShopAdapter,
  InforShopParamsRoute,
  InforShopProps,
  InforShopState,
  INFORSHOP_ROUTER,
} from "../model";
import TabInforItem from "./tab-infor-item/tab-infor-item";
import TabInforShop from "./tab-infor-shop/tab-infor-shop";

export default class InforShopContainer extends React.PureComponent<
  InforShopProps,
  InforShopState
> {
  static start(params?: InforShopParamsRoute) {
    pushToPage(InforShopScreen, params);
  }

  private adapter: InforShopAdapter;

  constructor(props: InforShopProps) {
    super(props);
    this.adapter = new InforShopAdapter(this);
    this.state = {
      index: 0,
      routes: INFORSHOP_ROUTER,
    };
  }

  private renderScene = ({ route }: any) => {
    switch (route.key) {
      case EInforShop.INFORSHOP:
        return <TabInforShop />;

      case EInforShop.INFORITEM:
        return <TabInforItem />;

      default:
        break;
    }

    return null;
  };

  private renderTabBar = (props: any) => {
    const { index, routes } = props.navigationState;

    return (
      <View style={styles.viewTabbar}>
        {routes.map((e, i) => {
          const isActive = index === i;
          return (
            <TouchableOpacity
              style={[styles.btnTab, isActive && styles.btnTabActive]}
              onPress={() => this.onIndexChange(i)}
            >
              <BaseText
                style={{
                  color: isActive ? R.colors.primaryColor : "#BDBCBC",
                  fontSize: 16,
                }}
                fontStyle="bold"
              >
                {e.title}
              </BaseText>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

  private onIndexChange = (index: number): void => {
    this.setState({ index });
  };

  public render(): React.ReactNode {
    return (
      <ContainerHeader showBackButton styleContainer={styles.container}>
        <TabView
          // @ts-ignore
          navigationState={this.state}
          renderScene={this.renderScene}
          renderTabBar={this.renderTabBar}
          onIndexChange={this.onIndexChange}
          initialLayout={styles.inititalLayout}
        />
      </ContainerHeader>
    );
  }
}

// styles
const styles = StyleSheet.create({
  container: {
    paddingBottom: DimensionUtils.getBottomSpace() + 20,
  },
  inititalLayout: {
    width: DimensionUtils.getScreenWidth(),
    height: 0,
  },
  viewTabbar: {
    flexDirection: "row",
    backgroundColor: "#F2F2F2",
    marginHorizontal: 16,
    padding: 2,
    borderRadius: 3,
  },
  btnTab: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    height: 32,
    borderRadius: 2,
  },
  btnTabActive: {
    backgroundColor: R.colors.white,
    shadowColor: "#000000",
    shadowOffset: { width: 0.5, height: 0.7 },
    shadowOpacity: 0.5,
    shadowRadius: 1,
    elevation: 1.5,
  },
});
