import ImagesComponent from "features/create-infor/view/components/images.component";
import ModalSelectImage from "features/create-infor/view/components/modal-select-image";
import ButtonBase from "libraries/button/button-base";
import BaseInput from "libraries/container/base-input";
import { onChoosePhoto } from "libraries/image-picker/GetImageAbum";
import * as React from "react";
import {
  View,
  StyleSheet,
  Text,
  ScrollView,
  Platform,
  Alert,
} from "react-native";
import ImageCropPicker from "react-native-image-crop-picker";
import { Media } from "types/media.type";
import { DimensionUtils } from "utils/DimensionUtils";

export interface Props {}
export interface State {
  shopName: string;
  shopAddr: string;
  shopPhone: string;
  businessItems: string;
  nameWebsite: string;
  nameFbPage: string;
  nameZaloPage: string;
  shopee: string;
  medias: Media[];
}
export default class TabInforShop extends React.PureComponent<Props, State> {
  private imageComponentRef = React.createRef<ImagesComponent>();

  private modalSelectImage = React.createRef<ModalSelectImage>();
  constructor(props: Props) {
    super(props);
    this.state = {
      shopName: "",
      shopAddr: "",
      shopPhone: "",
      businessItems: "",
      nameWebsite: "",
      nameFbPage: "",
      nameZaloPage: "",
      shopee: "",
      medias: [],
    };
  }

  private onDeleteImagePressed = (index: number) => () => {
    const medias = [...this.state.medias];
    medias.splice(index, 1);
    console.log(medias);
    this.setState({ medias: [...medias] });
  };

  private onSelectImagePressed = () => {
    if (Platform.OS === "android") {
      this.modalSelectImage.current?.onOpen();
    } else {
      Alert.alert("Chọn ảnh", "Chọn ảnh cửa hàng", [
        {
          text: "Hủy bỏ",
          style: "cancel",
        },
        { text: "Chụp ảnh", onPress: this.onSelectCamera },
        { text: "Ảnh thư viện", onPress: this.onSelectAlbum },
      ]);
    }
  };

  private onSelectCamera = () => {
    this.modalSelectImage.current?.onClose();
    // onOpenCamera().then(async (item: any) => {
    //     // showLoading();
    //     const _id = Date.now().toString();
    //     const media: any = {
    //         path:
    //             Platform.OS === 'android'
    //                 ? `file://${item.path || item.uri}`
    //                 : item.path || item.uri,
    //         local: true,
    //         _id,
    //         name: item.fileName || `${_id}.JPG`
    //     };
    //     // hideLoading();
    //     // const uploadResult = await apis.postUploadImage(media);
    //     // this.setState({ medias: [...this.state.medias, uploadResult[0]] });
    // });
  };

  private onSelectAlbum = () => {
    const maxFile = 20 - this.state.medias.length;
    onChoosePhoto(maxFile).then((photos: any) => {
      console.log("photos: ", photos);
      let medias: any = [];
      return new Promise((resolve, reject) => {
        photos.map(async (item: any, index: number) => {
          const _id = Date.now().toString();
          const media: any = {
            path: Platform.OS === "android" ? `file://${item.path}` : item.path,
            local: true,
            _id,
            name: item.filename || `${_id}.JPG`,
          };
          // const uploadResult = await apis.postUploadImage(media);
          // medias.push(uploadResult[0]);
          medias.push(media);
          if (medias.length === photos.length) {
            resolve(medias);
          }
        });
        this.modalSelectImage.current?.onClose();
      }).then((medias: any) => {
        console.log("mediasaaaaaaaa: ", medias);
        this.setState({ medias: [...this.state.medias, ...medias] });
        this.modalSelectImage.current?.onClose();
        // hideLoading();
      });
    });
  };

  onChangeText = (type: string) => (value: string) => {
    switch (type) {
      case "shopName":
        this.setState({ shopName: value });
        break;
      case "shopAddr":
        this.setState({ shopAddr: value });
        break;
      case "shopPhone":
        this.setState({ shopPhone: value });
        break;
      case "nameWebsite":
        this.setState({ nameWebsite: value });
        break;
      case "nameFbPage":
        this.setState({ nameFbPage: value });
        break;
      case "nameZaloPage":
        this.setState({ nameZaloPage: value });
        break;
      case "shopee":
        this.setState({ shopee: value });
        break;
      default:
        break;
    }
  };

  onSave = () => {
    const {
      shopAddr,
      shopName,
      shopPhone,
      shopee,
      nameFbPage,
      nameWebsite,
      nameZaloPage,
      medias,
    } = this.state;
   const body :any={}
   body.shopName = shopName
   body.shopAddr = shopAddr
   body.shopPhone = shopPhone
   body.nameWebsite = nameWebsite
   body.nameFbPage = nameFbPage
   body.nameZaloPage = nameZaloPage
   body.shopee = shopee
   body.medias = medias
    console.log('body: ', body);
  };

  public render() {
    const {
      shopAddr,
      shopName,
      shopPhone,
      shopee,
      businessItems,
      nameFbPage,
      nameWebsite,
      nameZaloPage,
    } = this.state;
    return (
      <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
        <BaseInput
          title="infor.shopName"
          value={shopName}
          onChangeText={this.onChangeText("shopName")}
          textInput
        />
        <BaseInput
          title="infor.shopAddr"
          value={shopAddr}
          onChangeText={this.onChangeText("shopAddr")}
          textInput
        />
        <BaseInput
          title="infor.shopPhone"
          value={shopPhone}
          onChangeText={this.onChangeText("shopPhone")}
          textInput
        />
        <BaseInput
          title="infor.nameWebsite"
          value={nameWebsite}
          onChangeText={this.onChangeText("nameWebsite")}
          textInput
        />
        <BaseInput
          title="infor.nameFbPage"
          value={nameFbPage}
          onChangeText={this.onChangeText("nameFbPage")}
          textInput
        />
        <BaseInput
          title="infor.nameZaloPage"
          value={nameZaloPage}
          onChangeText={this.onChangeText("nameZaloPage")}
          textInput
        />
        <BaseInput
          title="infor.shopee"
          value={shopee}
          onChangeText={this.onChangeText("shopee")}
          textInput
        />
        <ImagesComponent
          ref={this.imageComponentRef}
          medias={this.state.medias}
          onDeleteImagePressed={this.onDeleteImagePressed}
          onSelectImagePressed={this.onSelectImagePressed}
          label="Ảnh của shop"
        />
        <ModalSelectImage
          ref={this.modalSelectImage}
          onLibrary={this.onSelectAlbum}
          onCamera={this.onSelectCamera}
        />
        <ButtonBase text="save" top={30} onPress={this.onSave} />
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    paddingBottom: DimensionUtils.getBottomSpace() + 20,
  },
});
