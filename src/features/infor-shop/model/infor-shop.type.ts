
/* 
  Created by TienPv at 08-05-2021 10:49:27
  Screen Thong tin ve shop
*/

import { RouterType } from "types/routes.type";

export interface InforShopParamsRoute {}

export interface InforShopProps {
  route: RouterType<InforShopParamsRoute>
}

export interface InforShopState {}

export enum EInforShop {
  INFORSHOP = 'Thông tin cửa hàng',
  INFORITEM = 'Sản phẩm kinh doanh'
}

export const INFORSHOP_ROUTER = [
  {
      key: EInforShop.INFORSHOP,
      title: EInforShop.INFORSHOP,
      id: 0
  },
  {
      key: EInforShop.INFORITEM,
      title: EInforShop.INFORITEM,
      id: 1
  }
];