/* 
  Created by dungnt at 04-07-2021 15:44:17
  Screen màn hình đầu tiên
*/

import * as React from 'react';
import { StyleSheet } from 'react-native';
import { SplashScreen } from 'routing/screen-name';
import { pushToPage } from 'routing/service-navigation';
import {
    SplashAdapter,
    SplashParamsRoute,
    SplashProps,
    SplashState
} from '../model';

export default class SplashContainer extends React.PureComponent<
    SplashProps,
    SplashState
> {
    static start(params?: SplashParamsRoute) {
        pushToPage(SplashScreen, params);
    }

    private adapter: SplashAdapter;

    constructor(props: SplashProps) {
        super(props);
        this.adapter = new SplashAdapter(this);
    }

    public componentDidMount(): void {
        this.adapter.navigateWithDelay();
    }

    public componentWillUnmount(): void {
        this.adapter.clearDelay();
    }

    public render(): React.ReactNode {
        return <></>;
    }
}

// styles
const styles = StyleSheet.create({});
