
/* 
  Created by dungnt at 04-07-2021 15:44:17
  Screen màn hình đầu tiên
*/

import { RouterType } from "types/routes.type";

export interface SplashParamsRoute {}

export interface SplashProps {
  route: RouterType<SplashParamsRoute>
}

export interface SplashState {}

