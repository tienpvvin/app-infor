/* 
  Created by dungnt at 04-07-2021 15:44:17
  Screen màn hình đầu tiên
*/
import { DATA_PROJECT_DEFAULT } from "constans/data";
import { GlobalVariable } from "constans/global-variable";
import { LoginScreen, MainScreen } from "routing/screen-name";
import { resetStack } from "routing/service-navigation";
import { AsyncStorageUtils, StorageKey } from "utils/AsyncStorageUtils";
import SplashContainer from "../view/splash.screen";

export class SplashAdapter {
  private view: SplashContainer;

  private timeout: any;

  constructor(view: SplashContainer) {
    this.view = view;
  }

  public navigateWithDelay = async (time = 1000) => {
    this.timeout = setTimeout(() => {
      this.navigateImmediately();
    }, time);
  };

  private async navigateImmediately() {
    GlobalVariable.projects = DATA_PROJECT_DEFAULT;
    const token = await AsyncStorageUtils.get(StorageKey.TOKEN);
    GlobalVariable.token = token;
    if (token) {
        resetStack(MainScreen);
    } else {
      resetStack(LoginScreen);
    }
  }

  public clearDelay(): void {
    if (!this.timeout) clearTimeout(this.timeout);
  }
}
