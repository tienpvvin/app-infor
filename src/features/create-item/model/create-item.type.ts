/* 
  Created by tienpv at 08-05-2021 11:46:07
  Screen thêm thông tin sản phẩm kinh doanh
*/

import { Media } from "types/media.type";
import { RouterType } from "types/routes.type";

export interface CreateItemParamsRoute {}

export interface CreateItemProps {
  route: RouterType<CreateItemParamsRoute>;
}

export interface CreateItemState {
  businessItems: string;
  price: string;
  medias: Media[]
}
