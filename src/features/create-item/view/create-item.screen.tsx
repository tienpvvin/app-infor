/* 
  Created by tienpv at 08-05-2021 11:46:07
  Screen thêm thông tin sản phẩm kinh doanh
*/

import ImagesComponent from "features/create-infor/view/components/images.component";
import ModalSelectImage from "features/create-infor/view/components/modal-select-image";
import ButtonBase from "libraries/button/button-base";
import BaseInput from "libraries/container/base-input";
import ContainerHeader from "libraries/container/container-header";
import * as React from "react";
import { StyleSheet, Text } from "react-native";
import { CreateItemScreen } from "routing/screen-name";
import { pushToPage } from "routing/service-navigation";
import {
  CreateItemAdapter,
  CreateItemParamsRoute,
  CreateItemProps,
  CreateItemState,
} from "../model";

export default class CreateItemContainer extends React.PureComponent<
  CreateItemProps,
  CreateItemState
> {
  static start(params?: CreateItemParamsRoute) {
    pushToPage(CreateItemScreen, params);
  }

  private adapter: CreateItemAdapter;

  private imageComponentRef = React.createRef<ImagesComponent>();

  private modalSelectImage = React.createRef<ModalSelectImage>();

  constructor(props: CreateItemProps) {
    super(props);
    this.adapter = new CreateItemAdapter(this);
    this.state = {
      businessItems: "",
      price: "",
      medias: [],
    };
  }
  private onDeleteImagePressed = (index: number) => () => {
    const medias = [...this.state.medias];
    medias.splice(index, 1);
    console.log(medias);
    this.setState({ medias: [...medias] });
  };

  private onSelectImagePressed = () => {
    if (Platform.OS === "android") {
      this.modalSelectImage.current?.onOpen();
    } else {
      Alert.alert("Chọn ảnh", "Chọn ảnh cửa hàng", [
        {
          text: "Hủy bỏ",
          style: "cancel",
        },
        { text: "Chụp ảnh", onPress: this.onSelectCamera },
        { text: "Ảnh thư viện", onPress: this.onSelectAlbum },
      ]);
    }
  };

  private onSelectCamera = () => {
    this.modalSelectImage.current?.onClose();
    // onOpenCamera().then(async (item: any) => {
    //     // showLoading();
    //     const _id = Date.now().toString();
    //     const media: any = {
    //         path:
    //             Platform.OS === 'android'
    //                 ? `file://${item.path || item.uri}`
    //                 : item.path || item.uri,
    //         local: true,
    //         _id,
    //         name: item.fileName || `${_id}.JPG`
    //     };
    //     // hideLoading();
    //     // const uploadResult = await apis.postUploadImage(media);
    //     // this.setState({ medias: [...this.state.medias, uploadResult[0]] });
    // });
  };

  private onSelectAlbum = () => {
    const maxFile = 20 - this.state.medias.length;
    ImageCropPicker.openPicker({
      multiple: true,
      maxFiles: 10,
      mediaType: "photo",
    })
      .then((images) => {
        console.log("images: ", images);
      })
      .catch((error) => {});
    // onChoosePhoto(maxFile);
    // onChoosePhoto(maxFile).then((photos: any) => {
    //     console.log('photos: ', photos);
    //     let medias: any = [];
    //     // showLoading();
    //     return new Promise((resolve, reject) => {
    //         photos.map(async (item: any, index: number) => {
    //             const _id = Date.now().toString();
    //             const media: any = {
    //                 path:
    //                     Platform.OS === 'android'
    //                         ? `file://${item.path}`
    //                         : item.path,
    //                 local: true,
    //                 _id,
    //                 name: item.fileName || `${_id}.JPG`
    //             };
    //             // const uploadResult = await apis.postUploadImage(media);
    //             // medias.push(uploadResult[0]);
    //             if (medias.length === photos.length) {
    //                 resolve(medias);
    //             }
    //         });
    //         this.modalSelectImage.current?.onClose();
    //     }).then((medias: any) => {
    //         this.setState({ medias: [...this.state.medias, ...medias] });
    //         this.modalSelectImage.current?.onClose();
    //         // hideLoading();
    //     });
    // });
  };
  public render(): React.ReactNode {
    const { businessItems, price } = this.state;
    return (
      <ContainerHeader showBackButton title={"Sản phẩm kinh doanh"}>
        <BaseInput
          title="infor.businessItems"
          value={businessItems}
          // onChangeText={this.onChangeTaskName}
          textInput
        />
        <BaseInput
          title="item.price"
          value={price}
          // onChangeText={this.onChangeTaskName}
          textInput
        />
        <ImagesComponent
          ref={this.imageComponentRef}
          medias={this.state.medias}
          onDeleteImagePressed={this.onDeleteImagePressed}
          onSelectImagePressed={this.onSelectImagePressed}
          label="Ảnh sản phẩm"
        />
        <ModalSelectImage
          ref={this.modalSelectImage}
          onLibrary={this.onSelectAlbum}
          onCamera={this.onSelectCamera}
        />
        <ButtonBase text="save" top={30} onPress={this.onSaveTask} />
      </ContainerHeader>
    );
  }
}

// styles
const styles = StyleSheet.create({});
