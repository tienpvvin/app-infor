/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* 
  Created by TienPv at 08-02-2021 18:29:38
  Screen Them thong tin nguoi dung
*/

import ButtonBase from "libraries/button/button-base";
import BaseInput from "libraries/container/base-input";
import ContainerHeader from "libraries/container/container-header";
import * as React from "react";
import { ScrollView, StyleSheet } from "react-native";
import { translate } from "res/languages";
import { CreateInforScreen } from "routing/screen-name";
import { pushToPage } from "routing/service-navigation";
import { DimensionUtils } from "utils/DimensionUtils";
import {
  CreateInforAdapter,
  CreateInforParamsRoute,
  CreateInforProps,
  CreateInforState,
} from "../model";

export default class CreateInforContainer extends React.PureComponent<
  CreateInforProps,
  CreateInforState
> {
  static start(params?: CreateInforParamsRoute) {
    pushToPage(CreateInforScreen, params);
  }

  private adapter: CreateInforAdapter;

  constructor(props: CreateInforProps) {
    super(props);
    this.adapter = new CreateInforAdapter(this);
    this.state = {
      husbandName: "",
      wifeName: "",
      husbandPhone: "",
      wifePhone: "",
      addr: "",
      email: "",
    };
  }

  componentDidMount() {
    this.adapter.getInfor();
  }

  onChangText = (type: string) => (value: string) => {
    switch (type) {
      case "husbandName":
        this.setState({ husbandName: value });
        break;
      case "wifeName":
        this.setState({ wifeName: value });
        break;
      case "husbandPhone":
        this.setState({ husbandPhone: value });
        break;
      case "wifePhone":
        this.setState({ wifePhone: value });
        break;
      case "addr":
        this.setState({ addr: value });
        break;
      case "email":
        this.setState({ email: value });
        break;
      default:
        break;
    }
  };
  onSave = () => {
    const { husbandName, wifeName, husbandPhone, wifePhone, addr, email } =
      this.state;
    this.adapter.postInfo({
      husbandName,
      wifeName,
      husbandPhone,
      wifePhone,
      addr,
      email,
    });
  };
  public render(): React.ReactNode {
    const { husbandName, wifeName, husbandPhone, wifePhone, addr, email } =
      this.state;
    return (
      <ContainerHeader
        showBackButton
        title={translate("infor.title")}
        styleContainer={styles.container}
      >
        <ScrollView>
          <BaseInput
            title="infor.husbandName"
            value={husbandName}
            onChangeText={this.onChangText("husbandName")}
            textInput
          />
          <BaseInput
            title="infor.wifeName"
            value={wifeName}
            onChangeText={this.onChangText("wifeName")}
            textInput
          />
          <BaseInput
            title="infor.husbandPhone"
            value={husbandPhone}
            onChangeText={this.onChangText("husbandPhone")}
            textInput
          />
          <BaseInput
            title="infor.wifePhone"
            value={wifePhone}
            onChangeText={this.onChangText("wifePhone")}
            textInput
          />
          <BaseInput
            title="infor.addr"
            value={addr}
            onChangeText={this.onChangText("addr")}
            textInput
          />
          <BaseInput
            title="infor.email"
            value={email}
            onChangeText={this.onChangText("email")}
            textInput
          />
          <ButtonBase text="save" top={30} onPress={this.onSave} />
        </ScrollView>
      </ContainerHeader>
    );
  }
}

// styles
const styles = StyleSheet.create({
  container: {
    paddingBottom: DimensionUtils.getBottomSpace() + 20,
  },
});
