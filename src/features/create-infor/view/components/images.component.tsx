/* eslint-disable react-native/no-inline-styles */
import BaseText from "libraries/container/base-text";
import * as React from "react";
import {
  FlatList,
  Image,
  ListRenderItem,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View,
} from "react-native";
import Config from "react-native-config";
import FastImage from "react-native-fast-image";
import R from "res/R";
import { Media, NewsConfig } from "types/media.type";
import NewsTypeLabelComponent from "./news-type-label.component";

interface Props {
  medias: Media[];
  label: string;
  onDeleteImagePressed: (index: number) => () => void;
  onSelectImagePressed: () => void;
}

interface State {
  newsConfig?: NewsConfig;
}

export default class ImagesComponent extends React.PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {};
  }

  setConfig = (newsConfig: NewsConfig) => {
    this.setState({ newsConfig });
  };

  private keyExtractor = (item: any): string => item._id;

  private renderItem: ListRenderItem<Media> = ({ item, index }) => {
    return (
      <View>
        <FastImage
          source={{
            uri: item.path,
          }}
          style={[styles.imageSize]}
        />

        <TouchableOpacity
          onPress={this.props.onDeleteImagePressed(index)}
          activeOpacity={0.8}
          style={styles.closeButtonWrapper}
        >
          <Image style={styles.imageCloseStyle} source={R.images.icCloseR} />
        </TouchableOpacity>
      </View>
    );
  };

  private renderFooter = () => {
    if (this.props.medias.length >= (this.state.newsConfig?.image || 1))
      return null;

    return (
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={this.props.onSelectImagePressed}
        style={[styles.imageSize, styles.footerWrapper]}
      >
        <Image
          style={{ width: 30, height: 30, resizeMode: "contain" }}
          source={R.images.ic_plus}
        />
      </TouchableOpacity>
    );
  };

  public render(): React.ReactNode {
    return (
      <View style={{ marginTop: 20, marginLeft: 20 }}>
        <NewsTypeLabelComponent
          // icon={R.images.ic_image_news}
          label={this.props.label}
        />

        <FlatList
          showsHorizontalScrollIndicator={false}
          horizontal
          style={{ marginTop: 15 }}
          data={this.props.medias}
          extraData={this.props.medias}
          ListFooterComponent={this.renderFooter}
          keyExtractor={this.keyExtractor}
          renderItem={this.renderItem}
        />

        <BaseText
          style={{
            marginTop: 8,
            color: R.colors.primaryColor,
            fontSize: 11,
          }}
        >
          Có thể đăng tối đa 20 ảnh
        </BaseText>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  imageCloseStyle: {
    width: 8,
    height: 8,
    tintColor: "red",
  },
  closeButtonWrapper: {
    padding: 5,
    borderRadius: 20,
    backgroundColor: "white",
    position: "absolute",
    right: 15,
    top: 5,
  },
  imageSize: {
    height: 100,
    width: 180,
    borderRadius: 10,
    marginRight: 10,
  },
  footerWrapper: {
    justifyContent: "center",
    alignItems: "center",
    borderStyle: "dashed",
    borderColor: "#F33736",
    borderWidth: 1,
  },
});
