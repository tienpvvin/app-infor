import BaseText from 'libraries/container/base-text';
import * as React from 'react';
import { Modal, StyleSheet, TouchableOpacity, View } from 'react-native';

export interface Props {
    onLibrary?: () => void;
    onCamera?: () => void;
}

export default class ModalSelectImage extends React.PureComponent<Props, any> {
    constructor(props: Props) {
        super(props);
        this.state = {
            visible: false
        };
    }

    onOpen = () => {
        this.setState({ visible: true });
    };

    onClose = () => {
        this.setState({ visible: false });
    };

    onLibrary = () => {
        const { onLibrary } = this.props;
        onLibrary && onLibrary();
    };

    onCamera = () => {
        const { onCamera } = this.props;
        onCamera && onCamera();
    };

    public render() {
        const { visible } = this.state;
        return (
            <Modal
                animationType="fade"
                transparent
                visible={visible}
                onRequestClose={this.onClose}
            >
                <TouchableOpacity
                    style={styles.container}
                    activeOpacity={1}
                    onPress={this.onClose}
                >
                    <TouchableOpacity
                        style={styles.viewModal}
                        onPress={this.onOpen}
                        activeOpacity={1}
                    >
                        <BaseText style={styles.header}>Chọn ảnh</BaseText>
                        <BaseText style={styles.text}>
                            Chọn ảnh bất động sản
                        </BaseText>
                        <View style={[styles.hr, { marginTop: 10 }]} />
                        <TouchableOpacity
                            style={styles.btn}
                            onPress={this.onLibrary}
                        >
                            <BaseText style={styles.text}>
                                ẢNH THƯ VIỆN
                            </BaseText>
                        </TouchableOpacity>
                        <View style={styles.hr} />
                        <TouchableOpacity
                            style={styles.btn}
                            onPress={this.onCamera}
                        >
                            <BaseText style={styles.text}>CHỤP ẢNH</BaseText>
                        </TouchableOpacity>
                        <View style={styles.hr} />
                        <TouchableOpacity
                            onPress={this.onClose}
                            style={styles.btn}
                        >
                            <BaseText style={styles.text}>HỦY BỎ</BaseText>
                        </TouchableOpacity>
                    </TouchableOpacity>
                </TouchableOpacity>
            </Modal>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(15,0,0,0.3)',
        flex: 1
    },
    viewModal: {
        width: '70%',
        backgroundColor: '#FFFFFF',
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'hidden',
        borderRadius: 12
    },
    header: {
        fontSize: 20,
        marginTop: 10
    },
    hr: {
        borderTopWidth: 0.5,
        borderColor: '#999999',
        width: '100%'
        // marginTop: 10
    },
    btn: {
        paddingVertical: 10
    },
    text: {
        fontSize: 16
    }
});
