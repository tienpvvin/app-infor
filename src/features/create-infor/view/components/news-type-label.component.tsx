/* eslint-disable react-native/no-inline-styles */
import BaseText from 'libraries/container/base-text';
import * as React from 'react';
import {
    Image,
    ImageSourcePropType,
    StyleProp,
    View,
    ViewStyle
} from 'react-native';

interface Props {
    icon?: ImageSourcePropType;
    label: string;
    containerStyle?: StyleProp<ViewStyle>;
}

interface State {}

export default class NewsTypeLabelComponent extends React.PureComponent<
    Props,
    State
> {
    constructor(props: Props) {
        super(props);
        this.state = {};
    }

    public render(): React.ReactNode {
        return (
            <View
                style={[
                    {
                        flexDirection: 'row',
                        alignItems: 'center'
                    },
                    this.props.containerStyle
                ]}
            >
                {this.props.icon && (
                    <Image
                        resizeMode="contain"
                        source={this.props.icon}
                        style={{ width: 20, height: 20, marginRight: 10 }}
                    />
                )}
                <BaseText
                    style={{
                        // textTransform: 'uppercase',
                        fontSize: 16,
                        color: '#555555'
                    }}
                >
                    {this.props.label}
                </BaseText>
            </View>
        );
    }
}
