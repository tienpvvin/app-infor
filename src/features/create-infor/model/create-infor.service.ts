import { BaseRes } from "types/base.type";
import ApiUtils from "utils/ApiUtils";

/* 
  Created by TienPv at 08-02-2021 18:29:38
  Screen Them thong tin nguoi dung
*/
const apiName = {
  GETINFOR:'customer/info'
};

export class CreateInfroService {
  async getInfor(): Promise<BaseRes> {
    return ApiUtils.fetch(apiName.GETINFOR,{},true)
      .then((response: any) => response)
      .catch((error) => error);
  }
}