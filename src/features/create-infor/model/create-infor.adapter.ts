/* 
  Created by TienPv at 08-02-2021 18:29:38
  Screen Them thong tin nguoi dung
*/
import { ApiStatus } from "types/base.type";
import CreateInforContainer from "../view/create-infor.screen";
import { CreateInfroService } from "./create-infor.service";
import { dataInfor } from "./create-infor.type";

export class CreateInforAdapter {
  private view: CreateInforContainer;
  private readonly service: CreateInfroService;
  constructor(view: CreateInforContainer) {
    this.view = view;
    this.service = new CreateInfroService();
  }

  getInfor = async (): Promise<boolean> => {
    const res = await this.service.getInfor();
    console.log("ressssss: ", res, res.customer.email);
    if (res.status_code === ApiStatus.SUCCESS) {
      this.view.setState({
        email: res.customer.email,
        husbandName: res.customer.husband_name,
        husbandPhone: res.customer.husband_phone,
        wifeName: res.customer.wife_name,
        wifePhone: res.customer.wife_phone,
        addr: res.customer.address,
      });
      return true;
    }
    return false;
  };

  postInfo = (body: any): Promise<boolean> => {
    console.log("body: ", body);
    // const res = await this.service.getInfor();
    // console.log("ressssss: ", res, res.customer.email);
    return false;
  };
}
