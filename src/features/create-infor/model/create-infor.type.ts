/* 
  Created by TienPv at 08-02-2021 18:29:38
  Screen Them thong tin nguoi dung
*/

import { Media } from "types/media.type";
import { RouterType } from "types/routes.type";

export interface CreateInforParamsRoute {}

export interface CreateInforProps {
  route: RouterType<CreateInforParamsRoute>;
}

export interface CreateInforState {
  husbandName: string;
  wifeName: string;
  husbandPhone: string;
  wifePhone: string;
  addr: string;
  email: string;
  // dataInfo: dataInfor;
  // shopName: string;
  // shopAddr: string;
  // shopPhone: string;
  // businessItems: string;
  // nameWebsite: string;
  // nameFbPage: string;
  // nameZaloPage: string;
  // shopee: string;
  // medias: Media[];
}
export interface dataInfor {
  husband_name: string;
  wife_name: string;
  husband_phone: string;
  wife_phone: string;
  address: string;
  email: string;
}
