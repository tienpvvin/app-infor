/* 
  Created by dungnt at 04-23-2021 17:16:00
  Screen todo
*/

import { RouterType } from 'types/routes.type';

export interface TodoParamsRoute {}

export interface TodoProps {
    route: RouterType<TodoParamsRoute>;
}

export interface TodoState {}

export enum ETodoTab {
    TODAY = 'To day',
    PROJECT = 'Project',
    DONE = 'Done',
    BY_DATE = 'By date'
}

export const TODO_ROUTER = [
    {
        key: ETodoTab.TODAY,
        title: ETodoTab.TODAY,
        id: 0
    },
    {
        key: ETodoTab.PROJECT,
        title: ETodoTab.PROJECT,
        id: 1
    },
    {
        key: ETodoTab.DONE,
        title: ETodoTab.DONE,
        id: 2
    },
    {
        key: ETodoTab.BY_DATE,
        title: ETodoTab.BY_DATE,
        id: 3
    }
];
