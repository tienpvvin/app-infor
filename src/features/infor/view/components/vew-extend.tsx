import BaseText from "libraries/container/base-text";
import BaseIcon from "libraries/icon/base-icon";
import React, { Component } from "react";
import {
  Dimensions,
  Image,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View,
} from "react-native";
import colors from "res/colors";
import R from "res/R";

const { height } = Dimensions.get("window");
const { width } = Dimensions.get("window");
export default class ViewExtend extends Component<any, any> {
  render() {
    return (
      <View style={styles.viewExtend}>
        <TouchableOpacity onPress={this.props.onPress} style={styles.buttonIF}>
          <Image
            source={this.props.source}
            style={[
              { width: 20, height: 22, marginLeft: -5 },
              this.props.styleIcon,
            ]}
          />
          <View style={styles.viewInbt}>
            <BaseText style={styles.fontText}>{this.props.text}</BaseText>
            <BaseIcon
              name="back"
              iconStyle={{ transform: [{ rotate: "-180deg" }] }}
              color={colors.blueGrey60}
              width={15}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  subConten2: {
    backgroundColor: R.colors.white,
    width: "100%",
    height: "100%",
    flexDirection: "column",
  },
  viewExtend: {
    backgroundColor: R.colors.white,
  },
  viewInbt: {
    flexDirection: "row",
    alignItems: "center",
    width: "90%",
    justifyContent: "space-between",
    height: "100%",
    marginLeft: 10,
  },
  buttonIF: {
    flexDirection: "row",
    width: width * 1 - 20,
    height: 50,
    paddingHorizontal: 10,
    marginHorizontal: 10,
    marginVertical: 5,
    alignItems: "center",
    borderRadius: 10,
    backgroundColor: R.colors.white,
  },
  subhead: {
    backgroundColor: R.colors.white,
    width: width * 1,
    height: height * 0.17,
    justifyContent: "flex-end",
    zIndex: -1,
    alignItems: "center",
    marginBottom: 20,
  },
  subHeadChild: {
    flexDirection: "row",
    width: width * 0.7,
    borderRadius: 5,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
  },

  button: {
    marginBottom: 15,
    width: 120,
    justifyContent: "center",
    alignItems: "center",
    height: 40,
    borderRadius: 40,
    borderWidth: 0.5,
    borderColor: R.colors.primaryColor,
  },
  iCon: {
    width: 18,
    height: 14,
    marginLeft: 15,
  },
  textname: {
    fontWeight: "500",
    marginTop: Platform.OS === "ios" ? "15%" : "11%",
    marginBottom: 10,
  },
  textlogout: {
    color: colors.primaryColor,
    fontSize: 15,
  },
  // button: {
  //   marginBottom: 15,
  //   width: 120,
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   height: 40,
  //   borderRadius: 40,
  //   borderWidth: 0.5,
  //   borderColor: R.colors.primaryColor
  // },
  iconStyle: {
    width: 19,
    height: 22,
  },
  fontText: {},
});
