/* eslint-disable react/no-unused-state */
/* eslint-disable react-native/no-inline-styles */
/* 
  Created by dungnt at 04-23-2021 17:16:00
  Screen todo
*/

import CreateInforContainer from "features/create-infor/view/create-infor.screen";
import InforShopContainer from "features/infor-shop/view/infor-shop.screen";
import TouchableOpacity from "libraries/button/touchable-opacity";
import BaseText from "libraries/container/base-text";
import ContainerHeader from "libraries/container/container-header";
import * as React from "react";
import { StyleSheet, View } from "react-native";
import { TabView } from "react-native-tab-view";
import colors from "res/colors";
import R from "res/R";
import { TodoScreen } from "routing/screen-name";
import { pushToPage } from "routing/service-navigation";
import { DimensionUtils } from "utils/DimensionUtils";
import {
  ETodoTab,
  TodoAdapter,
  TodoParamsRoute,
  TodoProps,
  TodoState,
  TODO_ROUTER,
} from "../model";
import ViewExtend from "./components/vew-extend";

export default class TodoContainer extends React.PureComponent<
  TodoProps,
  TodoState
> {
  static start(params?: TodoParamsRoute) {
    pushToPage(TodoScreen, params);
  }

  private adapter: TodoAdapter;

  constructor(props: TodoProps) {
    super(props);
    this.adapter = new TodoAdapter(this);
    this.state = {
      index: 0,
      routes: TODO_ROUTER,
    };
  }

  onInfor = () => {
    CreateInforContainer.start();
  };

  inforShop = () => {
    InforShopContainer.start();
  };
  public render(): React.ReactNode {
    return (
      <ContainerHeader title="Thông tin cá nhân">
        <ViewExtend
          onPress={this.onInfor}
          source={R.images.ic_user}
          text={"Thong tin ca nhan"}
        />
        <ViewExtend
          onPress={this.inforShop}
          source={R.images.ic_shop}
          text={"Thong tin cua hang"}
          styleIcon={{ width: 18, height: 22 }}
        />
        <ViewExtend
          //   onPress={() => {
          //     index ? NavigationService.navigate("PersonalIF") : this.showModal();
          //   }}
          source={R.images.ic_password}
          text={"Doi mat khau"}
          styleIcon={{ width: 18, height: 22 }}
        />
        <View
          style={{
            backgroundColor: R.colors.white,
            paddingTop: 20,
            alignItems: "center",
          }}
        >
          <TouchableOpacity
            style={styles.button}
            // onPress={this.onLogOut}
          >
            <BaseText style={styles.textlogout}>{"Dang xuat"}</BaseText>
          </TouchableOpacity>
        </View>
      </ContainerHeader>
    );
  }
}

// styles
const styles = StyleSheet.create({
  button: {
    marginBottom: 15,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 40,
    borderWidth: 1,
    paddingHorizontal: 30,
    paddingVertical: 10,
    borderColor: R.colors.primaryColor,
    backgroundColor: R.colors.white,
  },
  textlogout: {
    color: colors.primaryColor,
    fontSize: 15,
  },
});
