/* 
  Created by TienPv at 08-05-2021 21:56:02
  Screen Đăng nhập
*/
import { GlobalVariable } from "constans/global-variable";
import { MainScreen } from "routing/screen-name";
import { resetStack } from "routing/service-navigation";
import { ApiStatus } from "types/base.type";
import { AsyncStorageUtils, StorageKey } from "utils/AsyncStorageUtils";
import { showAlertError, showAlertSuccess } from "utils/CommonUtils";
import LoginContainer from "../view/login.screen";
import { LoginService } from "./login.service";
import { bodyLogin } from "./login.type";

export class LoginAdapter {
  private view: LoginContainer;
  private readonly service: LoginService;
  constructor(view: LoginContainer) {
    this.view = view;
    this.service = new LoginService();
  }

  async onLogin(body: bodyLogin): Promise<boolean> {
    const res = await this.service.login(body);
    if (res.status_code === ApiStatus.SUCCESS) {
      AsyncStorageUtils.save(StorageKey.TOKEN, res.access_token);
      GlobalVariable.token = res.access_token;
      showAlertSuccess("success.login");
      resetStack(MainScreen);
    } else {
      showAlertError("error.login");
    }
    return true;
  }
}
