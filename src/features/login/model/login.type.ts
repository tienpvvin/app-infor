/* 
  Created by TienPv at 08-05-2021 21:56:02
  Screen Đăng nhập
*/

import { RouterType } from "types/routes.type";

export interface LoginParamsRoute {}

export interface LoginProps {
  route: RouterType<LoginParamsRoute>;
}

export interface LoginState {}

export interface bodyLogin {
  username: string;
  password: string;
}
