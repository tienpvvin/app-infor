/* 
Created by TienPv at 08-05-2021 21:56:02
Screen Đăng nhập
*/
import { BaseRes } from "types/base.type";
import ApiUtils from "utils/ApiUtils";
import { bodyLogin } from "./login.type";
const apiName = {
  LOGIN: "login",
};

export class LoginService {
  async login(body: bodyLogin): Promise<BaseRes> {
    return ApiUtils.post(apiName.LOGIN, body)
      .then((response: any) => response)
      .catch((error) => error);
  }
}
