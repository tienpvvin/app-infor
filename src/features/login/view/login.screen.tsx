/* 
  Created by TienPv at 08-05-2021 21:56:02
  Screen Đăng nhập
*/

import ButtonBase from "libraries/button/button-base";
import ContainerHeader from "libraries/container/container-header";
import BaseIcon from "libraries/icon/base-icon";
import InputComponent from "libraries/input/input.component";
import * as React from "react";
import {
  Keyboard,
  ScrollView,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import R from "res/R";
import { LoginScreen } from "routing/screen-name";
import { pushToPage } from "routing/service-navigation";
import {
  LoginAdapter,
  LoginParamsRoute,
  LoginProps,
  LoginState,
} from "../model";

export default class LoginContainer extends React.PureComponent<
  LoginProps,
  LoginState
> {
  static start(params?: LoginParamsRoute) {
    pushToPage(LoginScreen, params);
  }

  private adapter: LoginAdapter;
  accountRef = React.createRef<InputComponent>();

  passwordRef = React.createRef<InputComponent>();
  constructor(props: LoginProps) {
    super(props);
    this.adapter = new LoginAdapter(this);
  }

  hideKeyboard = () => {
    Keyboard.dismiss();
  };
  onLogin = () => {
    const { value: username } = this.accountRef.current!.getValue();
    const { value: password } = this.passwordRef.current!.getValue();
    this.adapter.onLogin({ username, password });
  };
  public render(): React.ReactNode {
    return (
      <ContainerHeader>
        <TouchableWithoutFeedback onPress={this.hideKeyboard}>
          <View>
            <View>
              <View style={styles.logoView}>
                <BaseIcon
                  name="ic_logo"
                  // width={LogoSize.width}
                  // height={LogoSize.height}
                  // iconStyle={styles.icon}
                  color={R.colors.blueGrey60}
                />
              </View>
              <View style={styles.form}>
                <InputComponent
                  ref={this.accountRef}
                  placeholder="account"
                  iconName="ic_user"
                  style={{}}
                  autoCapitalize="none"
                  iconColor={R.colors.blueGrey60}
                />
                <InputComponent
                  ref={this.passwordRef}
                  placeholder="password"
                  iconName="ic_key"
                  secureTextEntry
                  autoCapitalize="none"
                  iconColor={R.colors.blueGrey60}
                />
                <ButtonBase text="login" onPress={this.onLogin} />
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </ContainerHeader>
    );
  }
}

// styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: R.colors.primaryColor,
  },
  form: {
    paddingHorizontal: 30,
    marginBottom: 20,
  },
  logoView: {
    width: 220,
    height: 220,
    backgroundColor: R.colors.white,
    borderRadius: 220 / 2,
    alignSelf: "center",
    marginVertical: 70,
    justifyContent: "center",
  },
});
