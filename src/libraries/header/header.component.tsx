import BaseText from 'libraries/container/base-text';
import * as React from 'react';
import { Image, StyleSheet, TouchableOpacity, View } from 'react-native';
import R from 'res/R';
import { goBack } from 'routing/service-navigation';
import { DimensionUtils } from 'utils/DimensionUtils';

interface Props {
    showBackButton?: boolean;
    title?: string;
    onPress?: () => void;
    renderRight?: JSX.Element;
    renderRightComponent?: JSX.Element | null;
}

export default class HeaderComponent extends React.PureComponent<Props> {
    private onBackPressed = (): void => {
        goBack();
    };

    public render(): React.ReactNode {
        const {
            showBackButton,
            title,
            renderRight,
            renderRightComponent
        } = this.props;
        return (
            <View style={styles.wrapper}>
                <View style={styles.contentWrapper}>
                    {showBackButton && (
                        <TouchableOpacity
                            onPress={this.props.onPress || this.onBackPressed}
                            style={styles.btnBackStyle}
                        >
                            <Image
                                resizeMode="contain"
                                style={styles.iconBackStyle}
                                source={R.images.ic_back}
                            />
                        </TouchableOpacity>
                    )}

                    {!!renderRight && (
                        <View style={styles.rightButtonWrapper}>
                            {renderRight}
                        </View>
                    )}
                    {!!renderRightComponent && renderRightComponent}

                    <BaseText
                        numberOfLines={1}
                        style={styles.titleLabelStyle}
                        fontStyle="bold"
                    >
                        {title || ''}
                    </BaseText>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    rightButtonWrapper: {
        width: 26,
        height: 26,
        position: 'absolute',
        right: 10,
        zIndex: 2
    },
    titleLabelStyle: {
        fontSize: 17,
        color: 'black',
        marginLeft: 40,
        marginRight: 40
    },
    iconBackStyle: {
        width: 26,
        height: 26,
        tintColor: 'black'
    },
    btnBackStyle: {
        paddingLeft: 10,
        left: 0,
        position: 'absolute',
        zIndex: 2
    },
    contentWrapper: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        marginTop: DimensionUtils.getStatusBarHeight()
    },
    wrapper: {
        width: '100%',
        height: 48 + DimensionUtils.getStatusBarHeight(),
        // backgroundColor: R.colors.white
    },
    viewCart: {
        right: 10,
        position: 'absolute',
        zIndex: 2
    }
});
