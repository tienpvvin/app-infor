/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/static-property-placement */
import * as React from 'react';
import {
    StyleProp,
    StyleSheet,
    Text,
    TextProps,
    TextStyle
} from 'react-native';

type FontType = 'bold' | 'light' | 'black' | 'normal';

export interface Props extends TextProps {
    style?: StyleProp<TextStyle>;
    fontStyle?: FontType;
    numberOfLines?: number;
    ellipsizeMode?: 'head' | 'middle' | 'tail' | 'clip';
    onPress?: () => void;
    onLongPress?: () => void;
    onTextLayout?: any;
}

export default class BaseText extends React.PureComponent<Props> {
    static defaultProps = {
        fontStyle: 'normal'
    };

    public render(): React.ReactChild {
        const { style, fontStyle, ellipsizeMode, children } = this.props;

        let textFontStyle = styles.fontRegular;

        switch (fontStyle) {
            case 'bold':
                textFontStyle = styles.fontBold;
                break;

            case 'light':
                textFontStyle = styles.fontLight;
                break;

            case 'black':
                textFontStyle = styles.fontSemibold;
                break;

            default:
                textFontStyle = styles.fontRegular;
                break;
        }

        return (
            <Text
                {...this.props}
                style={[textFontStyle, style]}
                ellipsizeMode={ellipsizeMode || 'tail'}
            >
                {children}
            </Text>
        );
    }
}

const styles = StyleSheet.create({
    fontBold: {
        fontFamily: 'Lato-Bold',
        fontSize: 14
    },
    fontRegular: {
        fontFamily: 'Lato-Regular',
        fontSize: 14
    },
    fontLight: {
        fontFamily: 'Muli-Light',
        fontSize: 14
    },
    fontSemibold: {
        fontFamily: 'Muli-Black',
        fontSize: 14
    }
});
