import * as React from 'react';
import {
    StyleSheet,
    TextInput,
    TextInputProps,
    TouchableOpacity,
    View
} from 'react-native';
import { translate } from 'res/languages';
import BaseText from './base-text';

interface Props extends TextInputProps {
    title: string;
    textInput?: boolean;
    onChange?: () => void;
}

interface State {}

export default class BaseInput extends React.Component<Props, State> {
    public render(): React.ReactNode {
        const { textInput, onChange } = this.props;
        if (textInput) {
            return (
                <View style={styles.container}>
                    <BaseText style={styles.title}>
                        {translate(this.props.title)}
                    </BaseText>
                    <TextInput style={styles.inputStyle} {...this.props} />
                </View>
            );
        }
        return (
            <View style={styles.container}>
                <BaseText style={styles.title}>
                    {translate(this.props.title)}
                </BaseText>
                <TouchableOpacity activeOpacity={1} onPress={onChange}>
                    <BaseText style={styles.inputStyle}>
                        {this.props.value}
                    </BaseText>
                </TouchableOpacity>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        marginTop: 10,
        marginHorizontal: 16
    },
    title: {
        fontSize: 16,
        color: '#555555'
    },
    inputStyle: {
        height: 48,
        borderRadius: 5,
        backgroundColor: '#F2F2F2',
        marginTop: 4,
        // padding: 0,
        paddingHorizontal: 16,
        paddingVertical: 10,
        color: 'black',
        fontSize: 16
    }
});
