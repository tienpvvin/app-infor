import HeaderComponent from 'libraries/header/header.component';
import * as React from 'react';
import { StyleProp, StyleSheet, View, ViewStyle } from 'react-native';
import R from 'res/R';

interface Props {
    showBackButton?: boolean;
    title?: string;
    renderRight?: JSX.Element;
    renderRightComponent?: JSX.Element | null;
    contentStyle?: ViewStyle;
    onPressBack?: () => void;
    styleContainer?: ViewStyle;
}

export default class ContainerHeader extends React.Component<Props> {
    public render(): React.ReactNode {
        return (
            <View style={[styles.container, this.props.styleContainer]}>
                <HeaderComponent
                    showBackButton={this.props.showBackButton}
                    renderRight={this.props.renderRight}
                    renderRightComponent={this.props.renderRightComponent}
                    title={this.props.title}
                    onPress={this.props.onPressBack}
                />

                <View style={[styles.contentStyle, this.props.contentStyle]}>
                    {this.props.children}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: R.colors.white,
        flex: 1
    },
    contentStyle: {
        backgroundColor: 'white',
        flex: 1,
        // borderTopLeftRadius: 20,
        overflow: 'hidden'
    }
});
