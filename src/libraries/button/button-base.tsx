/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prefer-const */
import BaseText from 'libraries/container/base-text';
import * as React from 'react';
import { StyleSheet, TouchableOpacity, ViewStyle } from 'react-native';
import FastImage from 'react-native-fast-image';
import { translate } from 'res/languages';
import R from 'res/R';
import { DimensionUtils } from 'utils/DimensionUtils';

interface Props {
    text?: string;
    name?: string;
    iconName?: string;
    widthIcon?: number;
    height?: number;
    width?: number | string;
    onPress?: () => void;
    backgroundColor?: string;
    flex?: number;
    colorText?: string;
    noRadius?: boolean;
    borderColor?: string;
    borderRadius?: number;
    style?: ViewStyle;
    fontSize?: number;
    marginLeft?: number;
    onlyBorder?: boolean;
    iconColor?: string;
    top?: number;
    disabled?: boolean;
}

const WIDTH_ICON = 18;
const HEIGH_BUTTON = 44;
const WIDTH_BUTTON = DimensionUtils.getScreenWidth() - 32;
const FONT_SIZE = 16;

export default class ButtonBase extends React.PureComponent<Props> {
    public render(): React.ReactNode {
        let {
            iconName,
            widthIcon,
            text,
            width,
            height,
            borderColor,
            onPress,
            backgroundColor,
            flex,
            colorText,
            noRadius,
            borderRadius,
            name,
            style,
            fontSize,
            marginLeft,
            onlyBorder,
            iconColor,
            top,
            disabled
        } = this.props;
        height = height || HEIGH_BUTTON;
        width = width || WIDTH_BUTTON;
        flex = flex || undefined;
        backgroundColor = backgroundColor || R.colors.primaryColor;
        borderColor = borderColor || 'transparent';
        borderRadius = borderRadius || 5;
        fontSize = fontSize || FONT_SIZE;
        marginLeft = marginLeft || 8;

        if (disabled) backgroundColor = '#FFD8B2';

        if (onlyBorder) {
            borderColor = R.colors.primaryColor;
            backgroundColor = 'white';
            colorText = R.colors.primaryColor;
        }

        return (
            <TouchableOpacity
                style={[
                    styles.container,
                    {
                        height,
                        width,
                        backgroundColor,
                        flex,
                        borderRadius: noRadius ? undefined : borderRadius,
                        borderColor,
                        marginTop: top || undefined
                    },
                    style
                ]}
                onPress={onPress}
                disabled={disabled || !onPress}
                activeOpacity={0.8}
            >
                {iconName && (
                    <FastImage
                        // @ts-ignore
                        source={R.images[iconName]}
                        resizeMode={FastImage.resizeMode.contain}
                        style={{
                            width: widthIcon || WIDTH_ICON,
                            height: widthIcon || WIDTH_ICON
                        }}
                        tintColor={iconColor || undefined}
                    />
                )}
                {!!text && (
                    <BaseText
                        style={[
                            styles.txtStyle,
                            {
                                color: colorText || R.colors.white,
                                fontSize,
                                marginLeft
                            }
                        ]}
                        fontStyle="bold"
                    >
                        {translate(text)}
                    </BaseText>
                )}
                {!!name && (
                    <BaseText
                        style={[
                            styles.txtStyle,
                            {
                                color: colorText || R.colors.white,
                                fontSize,
                                marginLeft
                            }
                        ]}
                        fontStyle="bold"
                    >
                        {name}
                    </BaseText>
                )}
            </TouchableOpacity>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        alignSelf: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        borderWidth: 0.8
    },
    txtStyle: {
        fontSize: 14,
        fontWeight: 'bold',
        marginLeft: 8
    }
});
