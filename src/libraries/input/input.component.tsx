import * as React from "react";
import {
  StyleProp,
  StyleSheet,
  TextInput,
  TextStyle,
  View,
  ViewStyle,
} from "react-native";
import { translate } from "res/languages";
import R from "res/R";
import BaseIcon from "libraries/icon/base-icon";

interface Props {
  placeholder: string;
  text?: string;
  style?: StyleProp<TextStyle>;
  containerStyle?: StyleProp<ViewStyle>;
  defaultValue?: string;
  placeholderTextColor?: string;
  iconName?: string;
  iconWidth?: number;
  iconHeight?: number;
  iconOnpress?: () => void;
  secureTextEntry?: boolean;
  iconColor?: string;
  autoCapitalize?: "none" | "sentences" | "words" | "characters";
}

interface State {
  value: string;
  isChange: boolean;
}

interface ValueRes {
  value: string;
  isChange: boolean;
}
export default class InputComponent extends React.PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      value: "",
      isChange: false,
    };
  }

  getValue = (): ValueRes => {
    const { value, isChange } = this.state;
    const { defaultValue } = this.props;
    return { isChange, value: isChange ? value.trim() : defaultValue || "" };
  };

  onChangeText = (value: string): void => {
    this.setState({ value, isChange: true });
  };

  public render(): React.ReactNode {
    const {
      style,
      placeholder,
      containerStyle,
      defaultValue = "",
      placeholderTextColor = R.colors.primaryColor,
      iconName,
      iconWidth = 15,
      iconHeight = 20,
      iconOnpress,
      secureTextEntry,
      iconColor,
      autoCapitalize,
    } = this.props;
    const { isChange, value } = this.state;
    return (
      <View style={[styles.viewStyle, containerStyle]}>
        {!!iconName && (
          <BaseIcon
            name={iconName}
            width={iconWidth}
            height={iconHeight}
            onPress={iconOnpress}
            iconStyle={styles.icon}
            color={iconColor}
          />
        )}
        <TextInput
          value={isChange ? value : defaultValue}
          style={[styles.input, style]}
          placeholder={translate(placeholder)}
          onChangeText={this.onChangeText}
          // placeholderTextColor={placeholderTextColor}
          secureTextEntry={secureTextEntry}
          autoCapitalize={autoCapitalize}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewStyle: {
    paddingHorizontal: 12,
    //paddingVertical: 10,
    borderRadius: 5,
    width: "100%",
    marginHorizontal: 20,
    alignSelf: "center",
    borderBottomWidth: 0.5,
    borderBottomColor: R.colors.primaryColor,
    marginBottom: 15,
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "center",
    alignContent: "center",
  },
  input: {
    padding: 0,
    paddingLeft: 15,
    color: R.colors.primaryColor,
    fontSize: 14,
    flex: 1,
    paddingVertical: 10,
    borderColor: "red",
  },
  icon: {
    marginBottom: 10,
    color: R.colors.blueGrey60,
  },
});
