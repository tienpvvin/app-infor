/* eslint-disable react/sort-comp */
import ButtonBase from 'libraries/button/button-base';
import BaseText from 'libraries/container/base-text';
import * as React from 'react';
import {
    Modal,
    StyleSheet,
    TouchableOpacity,
    View,
    ViewStyle
} from 'react-native';
import { translate } from 'res/languages';
import R from 'res/R';
import { hideKeyboard } from 'utils/CommonUtils';
import { DimensionUtils } from 'utils/DimensionUtils';

interface Props {
    onPressYes?: () => void;
    onPressNo?: () => void;
    contentStyle?: ViewStyle;
    disableRight?: boolean;
}

interface OptionModalYesNo {
    content?: string;
    labelLeft?: string;
    labelRight?: string;
    contentTxt?: string;
    title?: string;
}

interface State {
    visible: boolean;
    option?: OptionModalYesNo;
}

export default class ModalYesNoComponent extends React.PureComponent<
    Props,
    State
> {
    constructor(props: Props) {
        super(props);
        this.state = {
            visible: false,
            option: undefined
        };
    }

    public open(option?: OptionModalYesNo | undefined): void {
        hideKeyboard();
        // @ts-ignore
        this.setState({ option, visible: true });
    }

    private onClose = (): void => {
        this.setState({ visible: false });
        this.props?.onPressNo?.();
    };

    private onPressYes = (): void => {
        this.setState({ visible: false }, () => {
            this.props?.onPressYes?.();
        });
    };

    private noop = (): void => {};

    public render(): React.ReactNode {
        const { visible, option } = this.state;
        return (
            <Modal
                animationType="fade"
                transparent
                visible={visible}
                onRequestClose={this.noop}
            >
                <View style={[styles.modalWrap, this.props.contentStyle]}>
                    <TouchableOpacity
                        onPress={this.onClose}
                        style={styles.modalOverlay}
                        activeOpacity={1}
                    />

                    <View style={[styles.modalView]}>
                        <BaseText style={styles.subtitle}>
                            {option?.content &&
                                translate(option && option.content)}
                        </BaseText>
                        {this.props.children}
                        <View style={styles.actionView}>
                            <ButtonBase
                                onlyBorder
                                onPress={this.onClose}
                                text={option?.labelLeft || 'modalYesNo.no'}
                                width={DimensionUtils.getScreenWidth() / 2 - 30}
                            />

                            <ButtonBase
                                text={option?.labelRight || 'modalYesNo.quit'}
                                width={DimensionUtils.getScreenWidth() / 2 - 30}
                                onPress={this.onPressYes}
                            />
                        </View>
                    </View>
                </View>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    modalWrap: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: R.colors.black60p
    },
    modalOverlay: {
        flex: 1,
        position: 'absolute',
        width: '100%',
        height: '100%'
    },
    modalView: {
        backgroundColor: R.colors.white,
        zIndex: 10,
        marginHorizontal: 12,
        borderRadius: 14,
        alignItems: 'center'
    },
    header: {
        height: 54,
        width: '100%',
        borderTopLeftRadius: 14,
        borderTopRightRadius: 14,
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        textAlign: 'center',
        fontSize: 18,
        color: R.colors.white,
        fontWeight: 'bold'
    },
    subtitle: {
        fontSize: 15,
        color: '#1F2834',
        textAlign: 'center',
        lineHeight: 20,
        paddingHorizontal: 20,
        marginBottom: 35,
        marginTop: 32,
        fontWeight: '600'
    },
    btn: {
        paddingVertical: 10,
        paddingHorizontal: 10,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: 56,
        backgroundColor: R.colors.primaryColor,
        borderBottomRightRadius: 12
    },
    txt: {
        color: R.colors.white,
        fontWeight: '600',
        fontSize: 16
    },
    actionView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '100%',
        paddingHorizontal: 11,
        marginBottom: 17
    },
    btnClose: {
        backgroundColor: R.colors.white,
        borderTopWidth: 1,
        borderTopColor: R.colors.primaryColor,
        borderBottomLeftRadius: 12
    }
});
