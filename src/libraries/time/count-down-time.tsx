/* eslint-disable radix */
import { GlobalVariable } from 'constans/global-variable';
import * as React from 'react';
import { StyleSheet, Text, TextStyle } from 'react-native';
import R from 'res/R';
import { sprintf } from 'sprintf-js';
import { DateTimeUtils } from 'utils/DateTimeUtils';

interface Props {
    until: number; // Tổng số giây còn lại
    txtTime?: TextStyle;
    onTimeEnd?: () => void;
}

interface State {
    until: number; // Tổng số giây còn lại
}

export default class CountDownTime extends React.Component<Props, State> {
    private timer: any;

    private timeEnd = 0;

    constructor(props: Props) {
        super(props);
        this.state = {
            until: GlobalVariable.pomodoroLength / 1000
        };
    }

    public componentDidMount(): void {
        // this.onSetUntil();
    }

    public componentWillUnmount(): void {
        this.onClearInterval();
    }

    public onSetUntil = (): void => {
        this.onClearInterval();

        const { date } = this.props;

        const dateEnd = date || new Date().toISOString();

        const numberEndTime = DateTimeUtils.string2Millis(dateEnd);

        const timeSpan = numberEndTime - Date.now();

        this.timeEnd = numberEndTime;
        this.setState({ until: Math.max(0, timeSpan / 1000) }, () => {
            if (this.state.until > 0) {
                this.timer = setInterval(this.updateTimer, 1000);
            }
        });
    };

    private onClearInterval = (): void => {
        if (this.timer) clearInterval(this.timer);
    };

    private updateTimer = () => {
        const { seconds, minutes } = this.getTimeLeft();

        if (parseInt(seconds) === 0 && parseInt(minutes) === 0) {
            this.onClearInterval();
            this.props?.onTimeEnd?.();
            // EventBus.getInstance().post({
            //     type: EventBusName.PRODUCT_FLASH_DEAL_END,
            //     payload: this.props.item._id
            // });

            return;
        }

        let until = Math.max(0, this.state.until - 1);

        const dateEnd = this.props.date || new Date().toISOString();

        const numberEndTime = DateTimeUtils.string2Millis(dateEnd);

        if (this.timeEnd !== numberEndTime) {
            until = (numberEndTime - Date.now()) / 1000;
        }

        this.setState({
            until: Math.max(0, until)
        });
    };

    private getTimeLeft = () => {
        const { until } = this.props;
        return {
            seconds: String(parseInt(String(until % 60), 10)),
            minutes: String(parseInt(String(until / 60), 10))
        };
    };

    public render(): React.ReactNode {
        const newTime = sprintf(
            '%02d:%02d',
            this.getTimeLeft().minutes,
            this.getTimeLeft().seconds
        ).split(':');

        return (
            <Text style={[styles.txtStyle, this.props.txtTime]}>
                {`${newTime[0]}:`}
                {newTime[1]}
            </Text>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        marginHorizontal: 10,
        marginTop: 12,
        backgroundColor: '#F8F8F8',
        borderRadius: 10,
        padding: 12,
        alignItems: 'center',
        shadowColor: R.colors.borderC,
        shadowOffset: { width: 0.5, height: 1 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        elevation: 2
    },
    title: {
        color: '#777777',
        fontSize: 15
    },
    date: {
        color: '#1F2834',
        fontWeight: 'bold'
    },
    txtTime: {
        color: '#1F2834',
        fontSize: 17,
        fontWeight: 'bold',
        marginTop: 3
    },
    txtStyle: {
        color: '#001C31',
        fontSize: 48,
        letterSpacing: 5,
        fontFamily: 'Lato-Bold'
    }
});
