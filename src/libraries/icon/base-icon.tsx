/* eslint-disable prefer-const */
import * as React from 'react';
import {
    ImageSourcePropType,
    ImageStyle,
    StyleProp,
    TouchableOpacity,
    ViewStyle
} from 'react-native';
import FastImage from 'react-native-fast-image';
import R from 'res/R';

interface BaseIconProps {
    icon?: ImageSourcePropType;
    name?: string | '';
    width?: number;
    height?: number;
    onPress?: () => void;
    style?: StyleProp<ViewStyle>;
    iconStyle?: StyleProp<ImageStyle>;
    color?: string;
    activeOpacity?: number;
    disable?: boolean;
}

const iconSize = 22;

export default class BaseIcon extends React.PureComponent<BaseIconProps> {
    static defaultProps: BaseIconProps = {
        activeOpacity: 0.8
    };

    render() {
        let {
            width,
            height,
            icon,
            name,
            onPress,
            style,
            iconStyle,
            color,
            activeOpacity,
            children
        } = this.props;
        if (!width) width = iconSize;
        if (!height) height = width;
        let disabled = false;
        if (this.props.disable) {
            disabled = this.props.disable;
        }
        if (onPress !== undefined) {
            return (
                <TouchableOpacity
                    onPress={onPress}
                    style={style}
                    activeOpacity={activeOpacity}
                    disabled={disabled}
                >
                    <FastImage
                        resizeMode="contain"
                        source={icon || R.images[name]}
                        style={[{ width, height }, iconStyle || {}]}
                        tintColor={color}
                    />
                    {children}
                </TouchableOpacity>
            );
        }
        return (
            <FastImage
                resizeMode="contain"
                source={icon || R.images[name]}
                style={[{ width, height }, iconStyle || {}]}
                tintColor={color}
            />
        );
    }
}
