import ImageCropPicker from 'react-native-image-crop-picker';
import ImagePicker, { ImagePickerOptions } from 'react-native-image-picker';

export function getImg() {
    const options = {
        title: 'Chọn ảnh',
        compressImageQuality: 0.7,
        takePhotoButtonTitle: 'Chụp ảnh',
        chooseFromLibraryButtonTitle: 'Chọn ảnh trong thư viện',
        cancelButtonTitle: 'Huỷ',
        maxHeight: 500,
        maxWidth: 500
    };
    return new Promise((resolve) => {
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                // console.log('User cancelled image picker');
            } else if (response.error) {
                // console.log('ImagePicker Error: ', response.error);
                return response.error;
            } else if (response.customButton) {
                // console.log('User tapped custom button: ', response.customButton);
                return response.customButton;
            } else {
                const source = { uri: response.uri };
                resolve(response);
            }
        });
    });
}

export const onChoosePhoto = (maxFile?: number) => {
    return new Promise((resolve, reject) => {
        ImageCropPicker.openPicker({
            multiple: true,
            compressImageQuality: 0.7,
            mediaType: 'photo',
            maxFiles: maxFile || 10
        }).then(async (images: any) => {
            resolve(images);
        });
    });
};

export const onOpenCamera = () => {
    const options: ImagePickerOptions = {
        quality: 0.7
    };
    return new Promise((resolve, reject) => {
        ImagePicker.launchCamera(options, (photo) => {
            resolve(photo);
        });
    });
};
