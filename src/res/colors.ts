const colors = {
  primaryColor: "#FF7D00",
  white: "#fff",
  white50: "rgba(255, 255, 255, 0.5)",
  borderC: "#ccc",
  borderD: "#ddd",
  borderE: "#eee",
  borderFE: "#FEFEFE",
  borderF9: "#F9F9F9",
  borderEF: "#EFEFEF",
  black: "#000",
  black1: "#111",
  black3: "#333333",
  black5: "#555",
  black7: "#777",
  black9: "#999",
  black10p: "#11111110",
  black40p: "rgba(0, 0, 0, 0.4)",
  black60p: "rgba(0, 0, 0, 0.6)",
  black70p: "rgba(0, 0, 0, 0.7)",
  blueGrey60: "#999999",
  transparent: "transparent",
};

export default colors;
