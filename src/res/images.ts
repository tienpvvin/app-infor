const images = {
    ic_back: require('../../assets/images/ic_back.png'),
    ic_tab_home: require('../../assets/images/ic_tab_home.png'),
    ic_tab_chart: require('../../assets/images/ic_tab_chart.png'),
    ic_tab_setting: require('../../assets/images/ic_tab_setting.png'),
    ic_tab_todo: require('../../assets/images/ic_tab_todo.png'),
    ic_close: require('../../assets/images/ic_close.png'),
    ic_play: require('../../assets/images/ic_play.png'),
    ic_music: require('../../assets/images/ic_music.png'),
    ic_pause: require('../../assets/images/ic_pause.png'),
    ic_check: require('../../assets/images/ic_check.png'),
    ic_del_project: require('../../assets/images/ic_del_project.png'),
    ic_pomo_chart: require('../../assets/images/ic_pomo_chart.png'),
    ic_pomo_done: require('../../assets/images/ic_pomo_done.png'),
    ic_image_news: require('./images/ic_image_news.png'),
    ic_plus: require('./images/ic_plus.png'),
    icCloseR: require('./images/icon_close.png'),
    back: require('./images/back.png'),
    ic_user: require('./images/ic_user.png'),
    ic_shop: require('./images/ic_shop.png'),
    ic_password: require('./images/ic_password.png'),
    ic_key: require('./images/ic_key.png'),
};

export default images;
