import images from './images';
import colors from './colors';
import fonts from './fonts';

const R = {
    images,
    colors,
    fonts
};

export default R;
