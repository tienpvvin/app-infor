export interface IProject {
    _id: string;
    name: string;
    color: string;
    totalTask?: number;
}
