export type TextTransform =
  | "none"
  | "capitalize"
  | "uppercase"
  | "lowercase"
  | undefined;

export enum ApiStatus {
  SUCCESS = 200,
  FAILED = 0,
  WATTING_APPROVE = 102,
}

export interface RenderItem<ItemT> {
  item: ItemT;
  index: number;
}

export type CbList<List> = (data?: List[]) => void;
export type CbItem<Item> = (data?: Item) => void;
export type CbVoid = () => void;
export interface BaseRes {
  status_code: ApiStatus;
  is_validate: IsValidate;
  msg: string;
  data: any;
  customer: any;
  access_token: string;
}
export enum IsValidate {
  VALID = 0,
  INVALID = 1,
}
