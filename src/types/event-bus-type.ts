import { ToastProps } from 'libraries/toast/toast.component';

export interface BaseEventType {
    type: EventBusName;
}

export interface BaseEvent<Payload> {
    type: EventBusName;
    payload: Payload;
}

export enum EventBusName {
    SHOW_TOAST_EVENT = 'SHOW_TOAST_EVENT',
    HIDE_TOAST_EVENT = 'HIDE_TOAST_EVENT',
    SUBSCRIPTION_SUCCESS = 'SUBSCRIPTION_SUCCESS',
    LOGIN_SUCCESS = 'LOGIN_SUCCESS'
}

export interface ToastPayloadType extends BaseEventType {
    payload: ToastProps;
}
