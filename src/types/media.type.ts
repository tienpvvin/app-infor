export interface Media {
    _id: string;
    url: string;
    thumbnail: string;
    thumbnail2x: string;
    type: MediaType;
    width: number;
    height: number;
    local?: boolean;
    path?: string;
    name?: string;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
}

export enum MediaType {
    PHOTO = 'PHOTO',
    VIDEO = 'VIDEO'
}
export interface NewsConfig {
    type: NewsConfigType;
    name: string;
    point: number;
    showingDays: number;
    notifyDays: number;
    image: number;
    description?: string;
}
export enum NewsConfigType {
    PAID_METHOD_1 = 'PAID_METHOD_1',
    PAID_METHOD_2 = 'PAID_METHOD_2',
    FREE_METHOD = 'FREE_METHOD',
    PAG = 'PAG',
    PROJECT = 'PROJECT'
}
