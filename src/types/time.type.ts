export interface ITime {
    _id: string;
    all: number;
    done: number;
    name: string;
}

export interface ITimeDay {
    _id: string;
    day: string;
}

export interface ITimeHour {
    _id: string;
    hour: string;
}
