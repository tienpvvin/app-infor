import { createStackNavigator } from '@react-navigation/stack';
import { CreateInforScreen } from 'features/create-infor';
import { CreateItemScreen } from 'features/create-item';
import { HomeScreen } from 'features/home';
import { InforShopScreen } from 'features/infor-shop';
import { LoginScreen } from 'features/login';
import { SplashScreen } from 'features/splash';
import * as React from 'react';
import * as ScreenName from 'routing/screen-name';
import HomeStackScreen from './home-bottom-tab';

const Stack = createStackNavigator();

function MainStack(): JSX.Element {
    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false
            }}
        >
            <Stack.Screen
                name={ScreenName.SplashScreen}
                component={SplashScreen}
            />
            <Stack.Screen
                name={ScreenName.MainScreen}
                component={HomeStackScreen}
            />
            <Stack.Screen
                name={ScreenName.LoginScreen}
                component={LoginScreen}
            />
            <Stack.Screen
                name={ScreenName.CreateInforScreen}
                component={CreateInforScreen}
            />
            <Stack.Screen
                name={ScreenName.CreateItemScreen}
                component={CreateItemScreen}
            />
            <Stack.Screen name={ScreenName.InforShopScreen} component={InforShopScreen} />
            <Stack.Screen name={ScreenName.HomeScreen} component={HomeScreen} />
        </Stack.Navigator>
    );
}

export default MainStack;
