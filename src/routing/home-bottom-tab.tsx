/* eslint-disable react-native/no-inline-styles */
import {
    BottomTabBarProps,
    createBottomTabNavigator
} from '@react-navigation/bottom-tabs';
import { ParamListBase, RouteProp } from '@react-navigation/core';
import { HomeScreen } from 'features/home';
import { TodoScreen } from 'features/infor';
import TouchableOpacity from 'libraries/button/touchable-opacity';
import React from 'react';
import { Image, StyleSheet, View } from 'react-native';
import R from 'res/R';
import { DimensionUtils } from 'utils/DimensionUtils';
import { navigateToPage } from './service-navigation';

export enum EBotomTab {
    HOME_TAB = 'HOME_TAB',
    CHART_TAB = 'CHART_TAB',
    TODO_TAB = 'TODO_TAB',
    SETTING_TAB = 'SETTING_TAB'
}

interface ScreenOptions {
    route: RouteProp<ParamListBase, string>;
    navigation: any;
    countUnreadNoti?: number;
}

const HomeStack = createBottomTabNavigator();

const onTabPressed = (screenName?: string) => (): void => {
    if (!screenName) return;
    navigateToPage(screenName);
};

const renderTabBar = (props: BottomTabBarProps) => {
    return (
        <View style={{ backgroundColor: 'white' }}>
            <View style={styles.viewBar}>
                {props.state.routes.map((e, i) => {
                    let icon: any = R.images.ic_tab_home;

                    const isActive = props.state.index === i;

                    switch (e.name) {
                        case EBotomTab.HOME_TAB:
                            icon = R.images.ic_tab_todo;
                            break;

                        case EBotomTab.TODO_TAB:
                            icon = R.images.ic_tab_home;
                            break;

                        // case EBotomTab.CHART_TAB:
                        //     icon = R.images.ic_tab_chart;
                        //     break;

                        // case EBotomTab.SETTING_TAB:
                        //     icon = R.images.ic_tab_setting;
                        //     break;

                        default:
                            break;
                    }

                    return (
                        <TouchableOpacity
                            key={e.name}
                            style={styles.btnTab}
                            onPress={onTabPressed(e.params?.screenName || '')}
                        >
                            <Image
                                source={icon}
                                style={[
                                    styles.icStyle,
                                    {
                                        tintColor: isActive
                                            ? R.colors.primaryColor
                                            : '#BDBCBC'
                                    }
                                ]}
                            />

                            <View
                                style={[
                                    styles.viewRound,
                                    {
                                        backgroundColor: isActive
                                            ? R.colors.primaryColor
                                            : R.colors.white
                                    }
                                ]}
                            />
                        </TouchableOpacity>
                    );
                })}
            </View>
        </View>
    );
};

function HomeStackScreen(): JSX.Element {
    return (
        <HomeStack.Navigator
            tabBarOptions={{
                showLabel: false
            }}
            tabBar={renderTabBar}
        >
            <HomeStack.Screen
                name={EBotomTab.HOME_TAB}
                component={HomeScreen}
                initialParams={{
                    screenName: EBotomTab.HOME_TAB
                }}
            />

            <HomeStack.Screen
                name={EBotomTab.TODO_TAB}
                component={TodoScreen}
                initialParams={{
                    screenName: EBotomTab.TODO_TAB
                }}
            />

            {/* <HomeStack.Screen
                name={EBotomTab.CHART_TAB}
                component={StatisticScreen}
                initialParams={{
                    screenName: EBotomTab.CHART_TAB
                }}
            />

            <HomeStack.Screen
                name={EBotomTab.SETTING_TAB}
                component={HomeScreen}
                initialParams={{
                    screenName: EBotomTab.SETTING_TAB
                }}
            /> */}
        </HomeStack.Navigator>
    );
}

export default HomeStackScreen;

const styles = StyleSheet.create({
    btnTab: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12
    },
    icStyle: {
        width: 30,
        height: 30
    },
    viewBar: {
        backgroundColor: 'white',
        borderRadius: 10,
        marginBottom:
            DimensionUtils.getBottomSpace() + DimensionUtils.ifIphoneX(0, 8),
        shadowColor: '#BDBCBC',
        shadowOffset: { width: 0.8, height: 1 },
        shadowOpacity: 1,
        shadowRadius: 2,
        elevation: 2,
        marginVertical: 2,
        marginHorizontal: 16,
        flexDirection: 'row'
    },
    viewRound: {
        width: 4,
        height: 4,
        borderRadius: 4,
        marginTop: 3
    }
});
