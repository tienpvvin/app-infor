export const MainScreen = 'MainScreen'; // Màn hình Màn hình trang chủ
export const HomeScreen = 'HomeScreen'; // Màn hình Màn hình trang chủ
export const SplashScreen = 'SplashScreen'; // Màn hình màn hình đầu tiên
export const TodoScreen = 'TodoScreen'; // Màn hình todo
export const CreateInforScreen = 'CreateInforScreen'; // Màn hình Them thong tin nguoi dung
export const InforShopScreen = "InforShopScreen"; // Màn hình Thong tin ve shop
export const CreateItemScreen = "CreateItemScreen"; // Màn hình thêm thông tin sản phẩm kinh doanh
export const LoginScreen = "LoginScreen"; // Màn hình Đăng nhập
