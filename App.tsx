/* eslint-disable @typescript-eslint/no-unused-expressions */
import { NavigationContainer } from '@react-navigation/native';
import RootComponent from 'libraries/container/root-component';
import ToastComponent from 'libraries/toast/toast.component';
import * as React from 'react';
import { Platform, StatusBar, UIManager } from 'react-native';
import MainStack from 'routing/main-navigation';
import { navigationRef } from 'routing/service-navigation';

interface AppProps {}

interface AppState {}

Platform.OS === 'android' && StatusBar.setBackgroundColor('transparent');

StatusBar.setBarStyle('dark-content');

StatusBar.setTranslucent(true);

export default class App extends React.Component<AppProps, AppState> {
    constructor(props: AppProps) {
        super(props);
        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental?.(true);
        }
    }

    public render(): React.ReactNode {
        return (
            <RootComponent>
                <NavigationContainer ref={navigationRef}>
                    <MainStack />
                </NavigationContainer>

                <ToastComponent />
            </RootComponent>
        );
    }
}
